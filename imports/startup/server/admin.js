Meteor.startup(function() {
	// Modules.server.startup() ;
	//Roles.addUsersToRoles(this.userId, 'admin');
	
	
	var users = [
				// {name:"Normal User",email:"normal@example.com",roles:[]},
				// {name:"View-Secrets User",email:"view@example.com",roles:['view-secrets']},
				// {name:"Manage-Users User",email:"manage@example.com",roles:['manage-users']},
				{name:"SP", username: "spodolski", email:"spodolski@viviohealth.com",roles:['admin']},
			];

	_.each(users, function (checkuser) {
		var id;

		var user = Meteor.users.findOne({'emails.address': checkuser.email});
		
		if (!user)
			return;
/* 			id = Accounts.createUser({
				email: checkuser.email,
				password: "apple1",
				profile: { name: checkuser.name }
			}); */
		else
			id = user._id
		
		if (Roles.userIsInRole(user._id, ['admin'], 'adm-group')) {
			return;
		}
		
		if (!checkuser.roles || checkuser.roles.length > 0) 
			console.log('users', user, checkuser);
			// Need _id of existing user record so this call must come
			// after `Accounts.createUser` or `Accounts.onCreate`
			Roles.addUsersToRoles(id, checkuser.roles, 'adm-group');
	});
});
