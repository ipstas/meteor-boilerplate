import { AccountsServer } from 'meteor/accounts-base';
import { Random } from 'meteor/random';

var formatUsername = function(name){
	name = name.replace(/ /g, '_').replace(/[^a-zA-Z0-9_]*/g,'');
	if (Meteor.users.findOne({username: name}))
		name = name + '_' + Random.secret(4);
	return name;
}

Accounts.onCreateUser(function(options, user) {
  var emails;
	if (options.profile) {
    user.profile = options.profile;
  }
	
	console.log('onCreateUser sending signup email', user);
/* 	Meteor.call('email.signup', user,(err, res) => { 
		console.log('onCreateUser email.signup sent', err, res);
	});	 */

	console.log('onCreateUser, fin', user);
  return user;
});

// Validate username, sending a specific error message on failure.
Accounts.validateNewUser((user) => {  
  // Ensure user name is long enough
	console.log('validateNewUser', user);

	if (user.services && (user.services.google || user.services.facebook || user.services.twitter))
		return true;
	
  if (!(/^[a-zA-Z0-9_-]{5,25}$/).test(user.username)) 
    throw new Meteor.Error(403, 'Your username needs at least 5 characters and use only Latin letters, numbers and symbols "-" and "_"');
  
	if ((/root\b|admin\b/).test(user.username))
		throw new Meteor.Error(403, 'Sorry, you can not use reserved names');

  var passwordTest = new RegExp("(?=.{6,}).*", "g");
  if (passwordTest.test(user.password) == false) 
    throw new Meteor.Error(403, 'Your password is too weak!');
  
  return true;
});