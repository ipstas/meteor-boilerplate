// import express from 'express';
// import snapsearch from 'snapsearch-client-nodejs';
	
/* import { Meteor } from 'meteor/meteor'
import { Picker } from 'meteor/meteorhacks:picker';
import { Collections } from '/imports/api/links/collections.js';

Meteor.startup(function () {
  process.env.HTTP_FORWARDED_COUNT = 1;
}); */

/* if (Meteor.isServer) {
	Inject.rawHead("loader", Assets.getText('/html/loader.html'));
} */

/* if (Meteor.isClient) {
	Meteor.startup(function() {
		setTimeout(function() {
			$("#inject-loader-wrapper").fadeOut(500, function() { $(this).remove(); });
		}, 500);
	});
} */

//Meteor.startup(() => {
/* Tracker.autorun(function () {		
	//var _id = location.pathname.split('/').pop();
	//var tour = Tours.findOne(_id);
	console.log("injecting head", process.env, Meteor.default_connection);
	//console.log("injecting head", _id, tour);
	//Inject.meta('head', textOrFunc, [res])
	Inject.meta('meta_og_title', '<meta property="og:title" content="Virtual 360 test">');
	Inject.meta('meta_og_img', '<meta property="og:image" content="Virtual 360 test">');
}); */

/* var spinner = '<div class="white animated lightSpeedIn text-center fontlarge48" style="color:white; width:100%; font-size: 10vw">Hack Your Resume to the TOP</div>';
var body = '<div id="injectloadingspinner" class="injectloading" style="height: 100vh; background: #010f1a;"><div class="injallcenter">' + spinner + '<div class="injectloading injloadingspinner"></div></div></div>';
var css = '<link rel="stylesheet" href="/css/inject.css">';
//var css = '<style type="text/css">' + csslogo + ' ' + cssspinner + '</style';

function consoleIn(params, req, icon, res){
	var env = __meteor_runtime_config__.ROOT_URL;
	console.log('\n\n*******************', '\ninjecting head', req.url, new Date(), '\n\nenv:', env, '\n\n');
	if (req.headers['referer']) 
		console.log(
		'\nuser-agent:', req.headers['user-agent'], 
		'\nreferer:', req.headers['referer'],
		'\nremote:', req.headers['x-forwarded-for'],
		'\n********************\n');	
	else
		console.log('\nheaders:', req.headers, '\n********************\n');	
	
	
	
	// if (req.url.match(/gclid/))
		// console.log('\nheaders:', req.headers, '\n********************\n');	

};

var injectIt = function(title, description, icon, jsonld, req){
	var env = __meteor_runtime_config__.ROOT_URL;
	title = title || 'Hack your resume with the superpower of AI';
	description = title || 'Hack your resume with the superpower of AI, each job application will get a customized and targeted resume';
	icon = icon || 'http://res.cloudinary.com/ufg/image/upload/v1505148987/logo/career-hack-7.jpg';
	Inject.rawHead('meta', '<meta name="msvalidate.01" content="94B3F70E5F916804BF7CB42D68F39422" />\n<meta property="fb:app_id" content="1877742739178201">\n<meta property="og:locale" content="en_US">\n<meta property="og:type" content="website">\n<meta property="twitter:card" content="summary_large_image">');
	Inject.rawHead('meta_og_type', '<meta property="og:type" content="website" >');
	Inject.rawHead('meta_og_title', '<meta property="og:title" content="' + title + '" >');
	Inject.rawHead('meta_og_description', '<meta property="og:title" content="' + description + '" >');
	Inject.rawHead('meta_og_img_w', '<meta property="og:image:width" content="1024">');
	Inject.rawHead('meta_og_img_h', '<meta property="og:image:height" content="512">');	
	Inject.rawHead('meta_og_img', '<meta property="og:image" content="' + icon + '">');
	Inject.rawHead('meta_tw_title', '<meta property="twitter:title" content="' + title + '" >');
	Inject.rawHead('meta_tw_desc', '<meta property="twitter:description" content="' + description + '" >');
	Inject.rawHead('meta_tw_img', '<meta property="twitter:image" content="' + icon + '">');	
	Inject.rawHead('injected_css', css);
	if (jsonld)
		Inject.rawHead('injected_jsonld', jsonld);

	Inject.rawBody('injected', body);
}

var snapsearchIt = function(){
	// var express = require('express');
	// var snapsearch = require('snapsearch-client-nodejs');

	var app = express();

	//by default the it will only intercept and return a response with only status, header location, and html body
	app.use(
		snapsearch.connect(
			new snapsearch.Interceptor(
				new snapsearch.Client('stanp@xlazz.com', '2m0Z3S4g3Gr710isohckFLD6vNJCd0E24wiFeAR25dWN03Kw8u', {}, function (error, debugging) {
								//mandatory custom exception handler for Client errors such as HTTP errors or validation errors from the API
								//exceptions will only be called in the event that SnapSearchClient could not contact the API or when there are validation errors
								//in production you'll just ignore these errors, but log them here, the middleware is a no-op and will just pass through, and will not halt your application
					if (error) console.warn('snapsearch err', error); 
					console.log('snapsearch debug', debugging); 
				}),
				new snapsearch.Detector()
			),
			function (data) {
					
				//optional customised response callback
				//if intercepted, this allows you to specify what kind of status, headers and html body to return
				//remember headers is in the format of [ { name: '', value: '' },... ]
				
				// unless you know what you're doing, the location header is most likely sufficient
				// if you are setting up gzip compression, see the heroku example https://github.com/SnapSearch/SnapSearch-Client-Node-Heroku-Demo
				var newHeaders = [];
				data.headers.forEach(function (header) {
					if (header.name.toLowerCase() === 'location') {
							newHeaders.push({name: header.name, value: header.value});
					}
				});

				return {
					status: data.status,
					headers: newHeaders,
					html: data.html
				};

			}
		)
	);


	app.get('/', function (request, response) {
		console.log('snapsearch request', request, response);
		response.send('Was not a robot and we are here inside app');
	});

	//app.listen(1337);	
}
 */
// Meteor.startup(function () {
  // snapsearchIt();
// });
/* 
Picker.route('/', function(params, req, res, next) {
	
	var desc, title, icon, meta;
	//snapsearchIt();
	
	desc = {};
	desc.title = 'Hack your resume with the superpower of AI' ;
	desc.description = 'Free Editor and Viewer of 360 Panoramic Virtual Tours and Panoramas';
	icon = 'http://res.cloudinary.com/ufg/image/upload/v1505188519/hackresume/resumes.jpg';
	
	var jsonLD = {
		"@context" : "http://schema.org",
		"@type" : "WebApplication",
		"SoftwareApplication": "Artificial Intellect, Big Data, Cloud, Resume, Superpower",
		"screenshot" : "http://res.cloudinary.com/ufg/image/upload/v1505188519/hackresume/resumes.jpg",
		"about" : "Superchanred resume hacking with Artificial Intellect and Big Data",
		"author" : "Orangry Inc",
		"name" : "Hack Resume",
		//"image": "https://res.cloudinary.com/ufg/image/upload/w_1024,q_auto,fl_progressive:steep,e_improve,c_scale/v1501100786/virgo/pushit/FB_share_jfjjjn.jpg",
		// "location" : {
			// "@type" : "Place",
			// "name" : "Hollywood Bowl"
		// }			
	}
	jsonLD = JSON.stringify(jsonLD); 
	jsonLD = '<script type="application/ld+json" dochead="1">' + jsonLD + '</script>';
	consoleIn(params, req, icon, res);
	injectIt(desc.title, desc.description, icon, jsonLD, req);
	
	next();
});

Picker.route('/user/:username', function(params, req, res, next) {
	var user, title, icon, meta;
	
  var user = Meteor.users.findOne({username: params.username});
	if (!user) {
		consoleIn(params, req, icon, res);
		res.writeHead(301, {Location: "/worldtours"});
		res.end();			
		console.warn('NONEXISTING user ', params, 'redirected to worldtours');
		next();
		return;
	}
	//console.log("injecting head tour", params, tour);
	
	title = 'Panoramic Virtual 360 author: ' + user.username;
	if (!user || !user.profile || !user.profile.avatar)		
		icon = domain + '/img/Batman-icon.png';
	else 
		icon = user.profile.avatar.replace('/upload', '/upload/' + Meteor.settings.public.CLOUDINARY_SMALLER);

	var jsonLD = {
		"@context" : "http://schema.org",
		"@type" : "Person",
		//"about": "360 Virtual Tours Author: " + user.username,
		"givenName" : user.profile.firstName,
		"familyName": user.profile.lastName,
		"image" : icon,
		"description": user.profile.description,
		"url": domain + '/user/' + user.username,
		// "location" : {
			// "@type" : "Place",
			// "name" : "Hollywood Bowl"
		// }			
	};
	jsonLD = JSON.stringify(jsonLD); 
	jsonLD = '<script type="application/ld+json" dochead="1">' + jsonLD + '</script>';
	
	consoleIn(params, req, icon, res);
	injectIt(title, title, icon, jsonLD, req);
  
	next();
});

Picker.route('/pricing', function(params, req, res, next) {
	var title = 'VR 360 panoramic tours pricing';
	var icon;
	var jsonLD = {
		"@context" : "http://schema.org",
		"@type" : "WebApplication",
		"SoftwareApplication": "Photography, Cloud, 360, Panoramic, Hosting",
		"screenshot" : "https://res.cloudinary.com/ufg/image/upload/w_1024,q_auto,fl_progressive:steep,e_improve,c_scale/v1501100786/virgo/pushit/FB_share_jfjjjn.jpg",
		"about" : "Free Editor and Viewer of 360 Panoramic Virtual Tours and Panoramas",
		"author" : "Orangry Inc",
		"name" : "Virgo 360 Panoramic Editor and Viewer",
		//"image": "https://res.cloudinary.com/ufg/image/upload/w_1024,q_auto,fl_progressive:steep,e_improve,c_scale/v1501100786/virgo/pushit/FB_share_jfjjjn.jpg",
		// "location" : {
			// "@type" : "Place",
			// "name" : "Hollywood Bowl"
		// }			
	}
	jsonLD = JSON.stringify(jsonLD); 
	jsonLD = '<script type="application/ld+json" dochead="1">' + jsonLD + '</script>';
	consoleIn(params, req, icon, res);
	injectIt(title, title, icon, jsonLD, req);
  //res.end();
	next();
});

Picker.route('/blog', function(params, req, res, next) {
	var title = 'VR 360 panoramic tours Blog';
	var icon = PushImages.findOne();

	var jsonLD = {
		"@context" : "http://schema.org",
		"@type" : "CreativeWork",
		"about": "360 Virtual Tours Blog",
		"headline": "Blog about 360 photography and 360 Virtual Tours",
		"author": "360 Zipper",
		"fileFormat": "Text",
		"url": domain + '/blog',
		"image": icon
	};
	jsonLD = JSON.stringify(jsonLD); 
	jsonLD = '<script type="application/ld+json" dochead="1">' + jsonLD + '</script>';
	
	consoleIn(params, req, icon, res);
	injectIt(title, title, icon, jsonLD, req);
  //res.end();
	//snapsearchIt();
	
	next();
});

Picker.route('/blog/post/:postid', function(params, req, res, next) {
	var title = 'VR 360 panoramic tours Blog';
	var icon;

	var post = PushIt.findOne({postid: params.postid});
	if (!post) {
		res.writeHead(301, {Location: "/blog"});
		res.end();	
		return console.warn('NONEXISTING blog postid', params);
	}
	if (post.image && post.image.length)
		icon = PushImages.findOne(post.image[0]);
	var jsonLD = {
		"@context" : "http://schema.org",
		"@type" : "CreativeWork",
		"about": "360 Virtual Tours Blog Post",
		"headline": post.title,
		"author": "360 Zipper",
		"fileFormat": "Text",
		"dateCreated": post.createdAt,
		"text" : post.text,
		"url": domain + '/blog/post/' + params.postid,
		"image": icon
	};
	jsonLD = JSON.stringify(jsonLD); 
	jsonLD = '<script type="application/ld+json" dochead="1">' + jsonLD + '</script>';
	
	consoleIn(params, req, icon, res);
	injectIt(title, title, icon, jsonLD, req);
  //res.end();
	//snapsearchIt();
	next();
});

Picker.route('/404', function(params, req, res, next) {
	var icon;
	consoleIn(params, req, icon);
	Inject.rawHead('meta_prerender', '<meta name="prerender-status-code" content="404">');
	injectIt();
	next();
});
 */