import { Email } from 'meteor/email';
import { Accounts } from 'meteor/accounts-base';

Meteor.startup(function() {

	Accounts.emailTemplates.siteName = "VirGO 360";
	Accounts.emailTemplates.from = "VirGO 360 Attendant <no-reply@xlazz.com>";
	Accounts.urls.resetPassword = function(token) {
		return Meteor.absoluteUrl('reset-password/' + token);
	};
	Accounts.urls.verifyEmail = function (token) {
		return Meteor.absoluteUrl('verify-email/' + token);
	};

	Accounts.urls.enrollAccount = function (token) {
		return Meteor.absoluteUrl('enroll-account/' + token);
	};

	Accounts.emailTemplates.enrollAccount.subject = function (user) {
			return "Welcome to VirGO 360, the best 360 viewer, " + user.profile.name;
	};
	Accounts.emailTemplates.enrollAccount.text = function (user, url) {
		 return "You have been selected to participate in building a better future!"
			 + " To activate your account, simply click the link below:\n\n"
			 + url;
	};
	Accounts.emailTemplates.resetPassword.from = function () {
		 // Overrides value set in Accounts.emailTemplates.from when resetting passwords
		 return "VirGO 360 Attendant <no-reply@xlazz.com>";
	};	
});