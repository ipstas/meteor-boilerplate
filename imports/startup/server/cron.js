import { CronJob } from 'cron';
import { Collections } from '/imports/api/links/collections.js';
import { Settings } from '/imports/api/links/collections.js';
var cron;
	
var job = new CronJob({
  cronTime: '00 30 11 * * 1-5',
	//cronTime: '*/10 * * * * 1-5',
  onTick: async function() {
    /*
     * Runs every second weekday (Monday through Friday)
     * at 11:30:00 AM. It does not run on Saturday
     * or Sunday.
     */
		 
		await Meteor.call('social.twitter.apppost',function(err,res){
			console.log('cron social.twitter.apppost', err, res);
		});
		await Meteor.call('social.telegram.apppost',function(err,res){
			console.log('cron social.telegram.apppost', err, res);
		});		
		console.log('cron: posting auto social');
  },
  start: false,
  timeZone: 'America/Chicago'
});

var job2 = new CronJob({
  cronTime: '00 30 11 */2 * 1-5',
	//cronTime: '*/10 * * * * 1-5',
  onTick: async function() {
    /*
     * Runs every second weekday (Monday through Friday)
     * at 11:30:00 AM. It does not run on Saturday
     * or Sunday.
     */
		 
		await Meteor.call('social.push.update',function(err,res){
			if (err) console.warn('ERR cron social.push.update apppost', err, res);
			console.warn('cron social.push.update apppost', err, res);
		});
		//console.log('cron: posting auto social');
  },
  start: false,
  timeZone: 'America/Chicago'
});

var job3 = new CronJob({
  cronTime: '00 00 */6 * * *',
	//cronTime: '*/10 * * * * 1-5',
  onTick: async function() {
    /*
     * Runs every second weekday (Monday through Friday)
     * at 11:30:00 AM. It does not run on Saturday
     * or Sunday.
     */
		 
		await Meteor.call('social.twitter.search', function(err,res){
			console.warn('\n\n*** \ncron social.twitter.search', err, res, '\n\n***');
		});
		//console.log('cron: posting auto social');
  },
  start: false,
  timeZone: 'America/Chicago'
});


Meteor.startup(function(){
	var cron4 = Settings.findOne({cron: {$exists: true}, job: 4});
	console.log('cron init', cron4, cron, Settings.findOne({cron:true}));
	
	if (cron4) {
		var job4 = new CronJob({
			cronTime: cron4.cron,
			//cronTime: '00 00 */6 * * *',
			onTick: async function() {	 
				await Meteor.call('social.medium.pull.search', {q: cron4.keyword});
				// , function(err,res){
					// console.log('\n\n*** \ncron social.medium.pull.search', err, res, '\n\n***');
				// });
				//console.log('cron: posting auto social');
			},
			start: false,
			timeZone: 'America/Chicago'
		});
		job4.start();
	}

	if (!process.env.ROOT_URL.match(/www.virgo/))	
		return;
	job.start();
	job2.start();
	job3.start();
	//job4.start();
	//console.log('cron: started', started);
});