// import tether from 'tether';
// global.Tether = tether;

import { FlowRouter } from 'meteor/kadira:flow-router';
//import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
//import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Accounts } from 'meteor/accounts-base';

import '../../ui/pages/not-found/not-found.js';

// Import needed templates
import '../../ui/layouts/layout.js';
import '../../ui/layouts/printlayout.js';
import '../../ui/pages/home.js';

// Set up all routes in the app
FlowRouter.route('/', {
  name: 'home',
  action(params, queryParams) {
		window.prerenderReady = false;
		
		var list = { main: 'home', footer: 'footer', nav: 'nav' };

		BlazeLayout.render('layout', list);		
		console.log('home', list, queryParams, document.location.hostname, navigator);

  },
});


FlowRouter.route('/adminpage', {
	triggersEnter: [AccountsTemplates.ensureSignedIn],
  name: 'adminpage',
  action() {
		if (Roles.userIsInRole(Meteor.userId(), 'admin', 'adm-group'))
			BlazeLayout.render('layout', { nav: 'navuser', main: 'adminpage' });
		// else
			// FlowRouter.go('/worldtours');
		console.log('user', Meteor.userId(), Meteor.user(), Roles.userIsInRole(Meteor.userId(), 'admin', 'adm-group'));
		if (window.ll) ll('tagScreen', name);
  },
});

/* 
FlowRouter.route('/user/', {
  name: 'user',
  action(params, queryParams) {
		Meteor.setTimeout(function(){
			if (Meteor.userId())
				if (Meteor.user()) 
					FlowRouter.go('/user/' + Meteor.user().username);
				else
					FlowRouter.go('/worldtours/');
			else
				FlowRouter.go('/sign-in/');
			console.log('route user redirect', Meteor.userId(), Meteor.user());
		},1000);
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/user/:username', {
  name: 'user',
  action(params, queryParams) {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'user' });
		console.log('route render /user/:username', params.username);
		if (window.ll) ll('tagScreen', name);
		var title = 'VirGO 360 user ' + params.username;
		SEO.set({
			title: title
		});
  },
});

FlowRouter.route('/pushit', {
  name: 'pushit',
  action() {
		if (!Roles.userIsInRole(Meteor.userId(), 'admin', 'adm-group'))
			FlowRouter.go('/worldtours');
		else 
			BlazeLayout.render('layout', { nav: 'navuser', main: 'pushit' });

		console.log('user', Meteor.userId(), Meteor.user(), Roles.userIsInRole(Meteor.userId(), 'admin', 'adm-group'));
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/blog', {
  name: 'blog',
  action() {
		window.prerenderReady = false;
		BlazeLayout.render('layout', { nav: 'navuser', main: 'blog' });

		//console.log('user', Meteor.userId(), Meteor.user(), Roles.userIsInRole(Meteor.userId(), 'admin', 'adm-group'));
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/blog/tags/:tag', {
  name: 'blog-tags',
  action() {
		window.prerenderReady = false;
		BlazeLayout.render('layout', { nav: 'navuser', main: 'blog' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/blog/post/:postid', {
  name: 'blogpost',
  action() {
		window.prerenderReady = false;
		BlazeLayout.render('layout', { nav: 'navuser', main: 'blogpost' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/faq', {
  name: 'faq',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'faq' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/pricing', {
  name: 'pricing',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'pricing' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/payment', {
  name: 'payment',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'payment' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/about', {
  name: 'about',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'about' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/profile', {
  name: 'profile',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'profile' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/user/', {
  name: 'user',
  action(params, queryParams) {
		Meteor.setTimeout(function(){
			if (Meteor.userId())
				if (Meteor.user()) 
					FlowRouter.go('/user/' + Meteor.user().username);
				else
					FlowRouter.go('/worldtours/');
			else
				FlowRouter.go('/sign-in/');
			console.log('route user redirect', Meteor.userId(), Meteor.user());
		},1000);
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/user/:username', {
  name: 'user',
  action(params, queryParams) {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'user' });
		console.log('route render /user/:username', params.username);
		if (window.ll) ll('tagScreen', name);
		var title = 'VirGO 360 user ' + params.username;
		SEO.set({
			title: title
		});
  },
});

FlowRouter.route('/feedback', {
  name: 'feedback',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'feedback' });
		if (window.ll) ll('tagScreen', name);
  },
}); */

FlowRouter.route('/soon', {
  name: 'coming',
  action() {
    BlazeLayout.render('layout', { nav: 'navuser', main: 'coming' });
		if (window.ll) ll('tagScreen', name);
  },
});

FlowRouter.route('/404', {
  name: '404',
  action() {
		console.log('404 route', this);
    BlazeLayout.render('layout404', { nav: 'nav', main: 'App_notFound' });
		if (window.ll) ll('tagScreen', name);

  },
});

FlowRouter.notFound = {
	name: 'redirect',
  action() {
		console.log('not found', FlowRouter.current(), this);
		FlowRouter.go('/404');
  },
};

// Accounts //
AccountsTemplates.configureRoute('signIn', {
  layoutType: 'blaze',
  name: 'signin',
  template: 'useraccount',
  layoutTemplate: 'layout',
	//redirect: '/worldtours',
  layoutRegions: {
    nav: 'nav',
    footer: 'footer'
  },
  contentRegion: 'main'
});

AccountsTemplates.configureRoute('signUp', {
  template: 'useraccount',
  layoutTemplate: 'layout',
	//redirect: '/worldtours',
  layoutRegions: {
    nav: 'nav',
    footer: 'footer'
  },
  contentRegion: 'main'
});


