import { Accounts } from 'meteor/accounts-base';

import './accounts.html';
// Options

var postSignUpHook = function(userId, info){
	var user, adds, username, emails;
	user = Meteor.users.findOne(userId);
	console.log('\n\npostSignUpHook 0', userId, 'info:', info, 'user:', user, '\n\n' );
  if (!user.username && user.services) {
		console.log('postSignUpHook 1', userId, info, user );
		if (user.services.facebook) {
			adds = user.services.facebook;
			if (!adds.email)
				adds.email = adds.id + '@facebook.com';
		}
		if (user.services.google)
			adds = user.services.google;
		if (adds) {
			if (adds.name)
				username = adds.name.replace(/ /g, '_').replace(/[^a-zA-Z0-9_]*/g,''); 
			else
				username = Random.secret(5);
			if (Meteor.users.findOne({username: username}))
				username = username + '_' + Random.secret(4);
			Meteor.users.update(userId,{$set: {username: username}});
			if (adds.picture) 
				Meteor.users.update(userId,{$set: {'user.profile.avatar': adds.picture}});
			
			if (adds.email){
				emails = {
					address: adds.email,
					verified: true
				}
				Meteor.users.update(userId,{$push: {emails: emails}});
			}
			console.log('postSignUpHook 2', adds, Meteor.users.findOne(userId),'\n\n');
		}			
		return true;
  }
};

var onSubmitHook = function(e, state){
  if (!e) {
		var path = FlowRouter.current().path;
		Modal.hide();
    if (state === "signIn") {
			console.log('user logged in 0', Meteor.user().username, path);
			// Meteor.setTimeout(()=>{
				// console.log('user logged in 300', Meteor.user().username, path);
				// FlowRouter.go(path);
			// },300);
      // Successfully logged in
      // ...
    }
    if (state === "signUp") {
			console.log('new user registered', Meteor.user());
			FlowRouter.setQueryParams({newuser: true});
			if (window.fbq) 
				fbq('track', 'CompleteRegistration', {
					value: 'Signed Up',
				});

			if (window.analytics) 		
				analytics.track("Signed Up", {
					category: "Account",
					label: 'sign up hook',
				});	
    }
  } else {
		console.error('onSubmitFunc error', e);
		// throw new Meteor.Error(500, 'onSubmitFunc error', e);
	}
};


AccountsTemplates.configure({
  defaultLayout: 'layout',
  defaultLayoutRegions: {
    nav: 'nav',
    footer: 'footer',
  },
  defaultContentRegion: 'main',
  showForgotPasswordLink: true,
  overrideLoginErrors: true,
  enablePasswordChange: true,

  // sendVerificationEmail: true,
  // enforceEmailVerification: true,
  //confirmPassword: true,
  //continuousValidation: false,
  //displayFormLabels: true,
  //forbidClientAccountCreation: true,
  //formValidationFeedback: true,
  //homeRoutePath: '/',
  //showAddRemoveServices: false,
  //showPlaceholders: true,

  negativeValidation: true,
  positiveValidation: true,
  negativeFeedback: false,
  positiveFeedback: true,
	
	homeRoutePath: '/user',
	redirectTimeout: 4000,
	postSignUpHook: postSignUpHook,
	onSubmitHook: onSubmitHook,
  // Privacy Policy and Terms of Use
  //privacyUrl: 'privacy',
  //termsUrl: 'terms-of-use',
});
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('verifyEmail');

var pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
  {
      _id: "username",
      type: "text",
      displayName: "username",
      required: true,
      minLength: 5,
			re: /^[a-zA-Z0-9_]{5,15}$/,
			errStr: 'latin letters and/or numbers only, i.e. Emily12'
  },
  {
      _id: 'email',
      type: 'email',
      required: true,
      displayName: "email",
      re: /.+@(.+){2,}\.(.+){2,}/,
      errStr: 'Invalid email',
  },
  {
      _id: 'username_and_email',
			placeholder: 'username or email',
      type: 'text',
      required: true,
      displayName: "Login",
  },
  pwd
]);

AccountsTemplates.addField({
  _id: 'terms',
  type: 'checkbox',
  template: "termsCheckbox",
  errStr: "You must agree to the Terms and Conditions",
  func: function(value) {
		console.log('termsCheckbox', value, this);
    return !value;
  },
  negativeValidation: false
});

AccountsTemplates.addField({
  _id: 'mailchimp',
	type: "checkbox",
	displayName: "Subscribe me to infrequent newsletter",
	value: true,
  //negativeValidation: false
});

/* AccountsTemplates.addField({
	_id: 'agree.checked',
	type: 'checkbox',
	displayName: "You have to agree to Terms to register",
	required: true,
	func: function(value){
		console.log('Agree', this, value);
		if (value) {
			return false;
		} else {
			if (Meteor.isClient)
				Bert.alert('You have to agree to Terms','warning');
			return true;
		}
	},
	errStr: 'You have to agree to Terms to register',
}); */

/* AccountsTemplates.addField({
	_id: 'agree.checked',
	type: 'checkbox',
	template: "termsCheckbox",
	displayName: "You have to agree to Terms to register",
	required: true,
	func: function(value){
		console.log('Agree', this, value);
		if (value) {
			return false;
		} else {
			if (Meteor.isClient)
				Bert.alert('You have to agree to Terms','warning');
			return true;
		}
	},
	errStr: 'You have to agree to Terms to register',
}); */

Template.atForm.onRendered( () => {
  $('#at-field-mailchimp').prop('checked', true);
});