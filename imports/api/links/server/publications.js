// All links-related publications
//import bodyParser from 'body-parser';
//import request from 'request';

import { Meteor } from 'meteor/meteor';
//import { Picker } from 'meteor/meteorhacks:picker';
//import { publishComposite } from 'meteor/reywood:publish-composite';

import { Druglist } from '../collections.js';

Meteor.publish('druglist', function(params) {
	var params = params || {};
	params.limit = params.limit || 16;
	
	var list = {};
		
	if (!Roles.userIsInRole(this.userId, ['admin'], 'adm-group')) 
		list = {_id: 1};
	else 	
		list = {};
	
	var data = Druglist.find(list, {sort: {createdAt: -1}, limit: params.limit});
	if (data.count())
		console.log('publush druglist', this.userId, data.fetch()[0].username, list, data.count(),'\n');
	return data;
});


// 							REST Api							//
/* Meteor.publish("api/druglist", function (key) {
	var nodata = Showtime.find({userId: 1});
	var data;
	process.env.NODE_ENV = 'development';
	data = Meteor.users.find({secretKey: key}, 
		{
			transform: function(doc){
				if (!doc.profile)
					doc.profile = {};
				if (!doc.profile.avatar)
					doc.profile.avatar = 'http://res.cloudinary.com/ufg/image/upload/avatar/batman.png';
				return doc;
			}
		},
		{fields:{username: 1, profile: 1}});
	console.log('user api', data.count(), key);
	if (!data)
		return nodata;
	return data;
	}, {
  url: "api/profile/:0",
  httpMethod: "get",
}); */

