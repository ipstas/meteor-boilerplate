// Methods related to links
//import moment from 'moment';
//import lodash from 'lodash';
//import {CronJob} from 'cron';

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Random } from 'meteor/random'
import { Email } from 'meteor/email'
import { HTTP } from 'meteor/http';

import { Collections } from './collections.js';
import { Druglist } from './collections.js';


// FUNCTIONS //


Meteor.methods({
  'checkUser'() {

		var users = [
					{name:"Admin User", username: "ipstas", email:"stanp@xlazz.com",roles:['admin']},
					{name: "Yana", username: "yana", email: "yana@kibirgov.net",roles:['admin']}
				];

		_.each(users, function (checkuser) {
			var id;
			
			var user = Meteor.users.findOne({'emails.address': checkuser.email});
			console.log('users', user, checkuser);
			
			if (!user)
				return;

			id = user._id
			
			if (Roles.userIsInRole(user._id, ['admin'], 'adm-group')) {
				return;
			}
			
			if (checkuser.roles && checkuser.roles.length > 0) 
				Roles.addUsersToRoles(id, checkuser.roles, 'adm-group');
				
		});
  },


});

