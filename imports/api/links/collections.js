import moment from 'moment';  
//import fse from 'fs-extra';
import { Mongo } from 'meteor/mongo';
//import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
import { AutoForm } from 'meteor/aldeed:autoform';

import { Random } from 'meteor/random';

export const Schemas = {};
export const Collections = {};

export const Druglist = Collections.Druglist = new Mongo.Collection('druglist');
export const Settings = Collections.Settings = new Mongo.Collection('settings');
export const Profiles = Collections.Profiles = new Mongo.Collection('profiles');

export const Texts = new Mongo.Collection('texts');
export const EmailsTmpl = new Mongo.Collection('emailstmpl');

////////////////////////////////////
Druglist.allow({
	insert: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
		// can only remove your own documents
		return false;	
	},
	update: function (userId, doc, fields, modifier) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
		// can only remove your own documents
		return false;
	},
	remove: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
		else 
			console.warn('unauth contact access remove', userId, doc);
		// can only remove your own documents
		return false;
	},
});
Schemas.SchemasDruglist = SchemasDruglist = new SimpleSchema({
  'type': {
    type: String,
  },
  'createdAt': {
    type: Date,
    label: 'Date',
/*     autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    }, */
/*     autoform: {
			omit: true,
    } */
  },
	"userId": {
    type: String,
		optional: true,
    regEx: SimpleSchema.RegEx.Id,
    autoValue: function () {
      if (Meteor.isClient && this.isInsert) {
        return Meteor.userId();
      }
    },
	},
  'username': {
    type: String,
		optional: true,
  },  
	'pushing': {
    type: String,
		optional: true,
  },
  'sentAt': {
    type: Date,
		optional: true,
	},  
	'sent': {
    type: Number,
    optional: true,
	},
	'msg':{
		type: Object,
		blackbox: true
	},
	'details':{
		type: Object,
		blackbox: true
	},
});

Schemas.SchemasSettings = SchemasSettings = new SimpleSchema({
  'type': {
    type: String,
  },
  'createdAt': {
    type: Date,
    label: 'Date',
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    },
/*     autoform: {
			omit: true,
    } */
  },
	"userId": {
    type: String,
		optional: true,
    regEx: SimpleSchema.RegEx.Id,
    autoValue: function () {
      if (Meteor.isClient && this.isInsert) {
        return Meteor.userId();
      }
    },
	},
});
//Settings.attachSchema(Schemas.SchemasSettings);
Settings.allow({
  insert: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  update: function (userId, doc) {
		//console.log('texts update', this.userId, doc.userId, userId);
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  remove: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  }
});

Schemas.SchemasTexts = SchemasTexts = new SimpleSchema({
  'title': {
    type: String,
		//unique: true,
  },
  'createdAt': {
    type: Date,
    label: 'Date',
/*     autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    }, */
/*     autoform: {
			omit: true,
      afFieldInput: {
        type: 'hidden'
      }
    } */
  },

});
//Texts.attachSchema(Schemas.SchemasTexts);
Texts.allow({
  insert: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  update: function (userId, doc) {
		//console.log('texts update', this.userId, doc.userId, userId);
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  remove: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  }
});

Schemas.SchemasEmails = SchemasEmails = new SimpleSchema({
  'id': {
    type: String,
/* 		autoform: {
			type: 'select',
			options: function (){
				return[
					{label:'adopter', value:'adopter'},
					{label:'signup', value:'signup'},
					{label:'question', value:'question'},
					{label:'complain', value:'complain'},
					{label:'other', value:'other'},
					{label:'liked', value:'liked'},
					{label:'menthioned', value:'menthioned'},
					{label:'paid', value:'paid'},
					{label:'cancelled', value:'cancelled'},
				]}
		} */
  },  
	'subject': {
    type: String,
  },
  'text': {
    type: String,
		optional: true,
/* 		autoform: {
			rows: 6,
		}, */
  },
  'createdAt': {
    type: Date,
    label: 'Date',
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    },
/*     autoform: {
			omit: true,
      afFieldInput: {
        type: 'hidden'
      }
    } */
  },
	"userId": {
    type: String,
		optional: true,
    regEx: SimpleSchema.RegEx.Id,
    autoValue: function () {
      if (this.isInsert) {
        return Meteor.userId();
      }
    },
/*     autoform: {
//			omit: true,
      options: function () {
        _.map(Meteor.users.find().fetch(), function (user) {
          return {
            label: user.emails[0].address,
            value: user._id
          };
        });
      }
    } */
	},
});
//EmailsTmpl.attachSchema(Schemas.SchemasEmails);
EmailsTmpl.allow({
  insert: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  update: function (userId, doc) {
		//console.log('texts update', this.userId, doc.userId, userId);
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  },
  remove: function (userId, doc) {
		if (Roles.userIsInRole(userId, ['admin'], 'adm-group')) 
			return true;
  }
});

Schemas.UserCountry = new SimpleSchema({
	name: {
		label: 'Country',
		type: String
	},
/*     code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    } */
});
Schemas.UserProfile = SchemasUserProfile = new SimpleSchema({
	firstName: {
		type: String,
		optional: true,
/* 		autoform:{
			label:false
		}		 */		
	},
	lastName: {
			type: String,
			optional: true,
/* 		autoform:{
			label:false
		}		 */	
	},
	description: {
			type: String,
			optional: true,
/* 		autoform:{
			rows:2
		}	 */		
	},	
  avatar: {
    type: String,
		optional: true,
/*     autoform: {
      afFieldInput: {
        type: 'cloudinary'
      }
    } */
  },
  logo: {
    type: String,
		optional: true,
/*     autoform: {
      afFieldInput: {
        type: 'cloudinary'
      }
    } */
  },
	birthday: {
			type: Date,
			optional: true,
/* 		autoform:{
			label:false
		}		 */					
	},
	gender: {
			type: String,
			allowedValues: ['Male', 'Female'],
			optional: true,
/* 		autoform:{
			label:false
		}		 */	
	},
	organization : {
		type: String,
		optional: true,
/* 		autoform:{
			label:false
		}	 */		
	},
	website: {
		type: String,
		regEx: SimpleSchema.RegEx.Url,
		optional: true,
/* 		autoform:{
			label:false
		}	 */						
	},
	phone: {
		type: String,
		optional: true,
/* 		autoform:{
			label:false
		}	 */						
	},	
	city: {
		type: String,
		optional: true,
/* 		autoform:{
			label:false
		}		 */	
	},	
	country: {
		type: Schemas.UserCountry,
		optional: true,
/* 		autoform:{
			label:false
		}	 */		
	},
  createdAt: {
    type: Date,
    label: 'Date',
/*     autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    },
    autoform: {
			omit: true,
      afFieldInput: {
        type: 'hidden'
      }
    } */
  },
});
Schemas.User = SchemasUser = new SimpleSchema({
    username: {
        type: String,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true,
				min: 4,
				max: 25,
/* 				custom: function () {
					if (Meteor.isClient && this.isSet) {
						console.log('username check', this.value);
						if (!(/^[a-zA-Z0-9_-]{5,25}$/).test(this.value)) {
							console.log('username check failed', this.value);
							return 'notAllowed';
						}			
					}
				} */
    },
    pro: {
        type: Boolean,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    emails: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
			type: Boolean,
/* 			autoform:{
				omit:true
			} */
    },
    // Use this registered_emails field if you are using splendido:meteor-accounts-emails-field / splendido:meteor-accounts-meld
    registered_emails: {
        type: Array,
        optional: true
    },
    'registered_emails.$': {
        type: Object,
        blackbox: true
    },
    createdAt: {
      type: Date,
			autoValue: function () {
				if (this.isInsert) {
					return new Date();
				}
			},
    },    
		visitedAt: {
      type: Date,
			optional: true
    },
    profile: {
        type: Schemas.UserProfile,
        optional: true
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
			type: Object,
			optional: true,
			blackbox: true,
/* 			autoform:{
					omit:true
			} */
    },
    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    roles: {
			type: Object,
			optional: true,
			blackbox: true,
/* 			autoform:{
				omit:true
			} */
    },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    // roles: {
        // type: Array,
        // optional: true
    // },
    // 'roles.$': {
        // type: String
    // },
    // In order to avoid an 'Exception in setInterval callback' from Meteor
    heartbeat: {
			type: Date,
			optional: true,
/* 			autoform:{
				omit:true
			} */
    }
});
//Meteor.users.attachSchema(Schemas.User);
Meteor.users.allow({
  insert: function (userId, doc) {
		//console.log('tours insert', userId, doc);
    // the user must be logged in, and the document must be owned by the user
    //return (userId && doc.userId === userId);
		return true;
  },
  update: function (userId, doc, fields, modifier) {
		//console.log('user update', userId, doc,fields, modifier);
    // can only change your own documents
    return doc._id === userId;
  },
  remove: function (userId, doc) {
		if (Roles.userIsInRole(this.userId, ['admin'], 'adm-group')) 
			return true;
    // can only remove your own documents
    return doc.userId === userId;
  },
});
