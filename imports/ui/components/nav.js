import './nav.html';
//import './status.js';
	
Template.nav.onCreated(function () {

});
Template.nav.onRendered(function () {

});
Template.nav.helpers({

});
Template.nav.events({

  'click .navbar-collapse ul li'(event,template) {
		var navMain = $(".navbar-main-collapse");
		navMain.collapse('hide');
  },
	'click .page-scroll'(e,t){
		console.log('clicked page scroll', e.target.hash, e, this);
		if (!e.target.hash)
			return;
		$("html, body").animate({scrollTop: $(e.target.hash).offset().top - 10}, 2000);
		FlowRouter.setQueryParams({href: e.target.hash});
		event.preventDefault();
	}
});

Template.logo.helpers({
	logo(){
		var logo = {};
		if (__meteor_runtime_config__.ROOT_URL.match(/virvir/) || document.URL.match(/site=vir/) ) {
			logo.url = '/img/up.png';
			logo.alt = 'Hack your resume 2';		
		}	
		else if (__meteor_runtime_config__.ROOT_URL.match(/zipper/) || document.URL.match(/site=re/)){
			logo.url = '/img/360zipper.png';
			logo.alt = '360 Zipper, panoramic VR tours for RE';
		}	
		else {
			logo.url = '/img/virgo360_transp_bckr.png';
			logo.alt = 'VirGO 360';				
		}			
		console.log('logo helper', __meteor_runtime_config__.ROOT_URL.match(/zipper/), document.URL.match(/site=re/), logo);
		return logo;	
	},
});
Template.logo.events({
	'click .page-scroll'(e,t){
		console.log('clicked page scroll', e.target.hash, e, this);
		if (! e.target.hash)
			return;
		$('html, body').stop().animate({
				scrollTop: $(e.target.hash).offset().top
		}, 1500, 'easeInOutExpo');
		event.preventDefault();
		//console.log('clicked page scroll 2', e.attr('href'));
	}
});

Template.navlanding.onCreated(function () {
	let t = Template.instance();
	t.adopters = new ReactiveVar ();
	Meteor.call('admin.adopters',(e,r)=>{
		t.adopters.set(r);
	});
});
Template.navlanding.helpers({
	visited(){
		return Session.get('visited') || Meteor.userId();
	},
	adopters(){
		let t = Template.instance();
		return t.adopters.get();
	}
	
});
Template.navlanding.events({
	'click .worldtours'(e,t){
		$('html, body').animate({scrollTop: $('body').offset().top -20 });	
	},
	'click .logout'(e,t){
		event.preventDefault();
		AccountsTemplates.logout();
		Meteor.setTimeout(function(){
			FlowRouter.go('/');
		},500);
	},
	'click' (e,t){
		console.log('navlanding click', e,t,this);
/* 		if (FlowRouter.current().context.hash)
			if ($("#"+FlowRouter.current().context.hash) && $("#"+FlowRouter.current().context.hash).offset())
				//$('html, body').animate({scrollTop: $("#"+FlowRouter.current().context.hash).offset().top -20 }, 'slow');
		else 
			//$('html, body').animate({scrollTop: $('body').offset().top -20 }, 'slow');		 */
	}
});

Template.navuser.onCreated(function () {
	let t = Template.instance();
	t.showMenu = new ReactiveVar ();
	t.showNews = new ReactiveVar ();
	t.autorun(function(){
		if (Meteor.userId())
			PostSubs.subscribe('userthings',{userId: Meteor.userId()});
	});
	t.subscribe('texts');
});
Template.navuser.onRendered(function () {
	$(".dropdown-toggle").dropdown();
});
Template.navuser.helpers({
	logo(){
		var logo = {};
		if (__meteor_runtime_config__.ROOT_URL.match(/virgo/)) {
			logo.url = '/img/virgo360_transp_bckr.png';
			logo.alt = 'VirGO 360';
		}	
		else if (__meteor_runtime_config__.ROOT_URL.match(/zipper/) || document.URL.match(/site=re/)){
			logo.url = '/img/360zipper.png';
			logo.alt = '360 Zipper, panoramic VR tours for RE';
		}	
		else {
			logo.url = '/img/up.png';
			logo.alt = 'Hack your resume 3';		
		}			
		return logo;	
	},
	showMenu(){
		console.log('showMenu', Template.instance().showMenu.get());
		return Template.instance().showMenu.get();
	},
	hidden(){
		if (!Template.instance().showMenu.get())
			return 'hidden';
	},
	newContact(){
		if (!Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group')) 
			return;
		Meteor.subscribe('contact');
		var data = Contact.findOne({seen: {$nin:[Meteor.userId()]}});
		return data;
	},
	newComment(){
		var data, tour, comments;
		data = Userthings.findOne({userId: Meteor.userId()});
		comments = Comments.findOne();
		if (Session.get('debug')) console.log('navuser newComment', data);
		if (data && data.tour_id && data.tour_id.length) {
			data.tour = Tours.findOne(data.tour_id[0]);
			return data;
		}
	},	
	productNews(){
		var data;
		data = Texts.findOne({title: 'productnews'},{sort:{createdAt: -1}});
		if (!data)
			return;
		console.log('productNews', data, Session.get('productnews'));
		if (Session.get('productnews') != data._id)
			return true;
	},	
	showNews(){
		let t = Template.instance();
		return t.showNews.get();
	},
	comment(){
		var subs;
		if (!this.tour_id || !this.tour_id.length)
			return;
		var data = Comments.findOne({tour_id: this.tour_id[0]});
		if (!data)
			subs = PostSubs.subscribe('comments',{tour_id: this.tour_id[0], caller: 'navuser'});
		if (Session.get('debug')) console.log('navuser comment', this.tour_id[0], subs, data, this);
		return data;
	},
	mobile(){
		console.log('window width', $(window).width() );
		if ($(window).width() < 768)
			return true;
	}

});
Template.navuser.events({
  'click #showMenu': function(e,t) {
		t.showMenu.set(!t.showMenu.get());
  },
	'click .logout': function(event){
		event.preventDefault();
		AccountsTemplates.logout();
	},
	'click .nav li'(){
		if ($('.navbar-collapse').hasClass('in'))
			$('.navbar-toggle').click();
	},
	'click .productNews'(e,t){
		t.showNews.set(!t.showNews.get());
	}
});

Template.navboxuser.onCreated(function(){
	this.active = new ReactiveVar();
});
Template.navboxuser.helpers({
	active(){
		return Template.instance().active.get();
	}
});
Template.navboxuser.events({
	'mouseenter  #menu-toggle': function(event,template) {
		template.active.set('active');
	},
/* 	'mouseleave  #menu-toggle': function(event,template) {
		template.active.set();
	}, */
	'click #menu-toggle': function(event,template) {
		// console.log('menu-toggle', event, template.active.get());
		template.active.set('active');
		event.preventDefault();
	},
	'click #menu-close': function(event,template) {
		//console.log('menu-toggle', event, template.active.get());
		template.active.set();
		event.preventDefault();
	},
  'click ul li': function(event,template) {
		console.log('ul li', event, template.active.get());
		// if (template.active.get('active'))
			template.active.set();
		// else
			// template.active.set('active');
		//event.preventDefault();
	},
	'click a'(e,t){
		console.log('clicked a',e,this);
		$('html, body').animate({scrollTop: $('body').offset().top - 10 }, 'slow');		
	}
});

