//Template.appAtInput.replaces("atInput");
import './accounts.html';

Template.termsCheckbox.events({
	'click #terms': function (event, template) {
		Modal.show('modalTerms');
	},
	'click': function(event, template){
		console.log('clicked checkbox', 	event);
		//sAlert.closeAll();
	},	
});

Template.inviteSign.events({
	'click .invitelink': function (event, template) {
		var href = 'http://www.360zipper.com';
		if (Meteor.isCordova)
			var ref = cordova.InAppBrowser.open(href, '_blank', 'location=yes');
		else
			window.open(href, '_blank');
	},
	'input': function (event, template) {
		console.log('clicked input', 	event);
		//sAlert.closeAll();		
	}
});
