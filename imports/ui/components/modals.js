//import { canvas } from 'blueimp-canvas-to-blob';
//import {Facebook, FacebookApiException} from 'fb';
import { Accounts } from 'meteor/accounts-base';
import { AutoForm } from 'meteor/aldeed:autoform';

import './modals.html';
import './collectform.js';
//import '/imports/external/reimg.js';

import {SelectText} from '/imports/functions/functions.js';
import { addingScene } from '/imports/functions/functions.js';
//import { dataURItoBlob } from '/imports/functions/functions.js';
import { blobToFile } from '/imports/functions/functions.js';
import {populateScene} from '/imports/functions/panofunctions.js';
import {populatePortals} from '/imports/functions/panofunctions.js';
window.populateScene = populateScene;
window.populatePortals = populatePortals;
//Images.attachSchema(SchemasImages);

import { Tours } from '/imports/api/links/collections.js';
import { Titles } from '/imports/api/links/collections.js';
import { Images } from '/imports/api/links/collections.js';
import { Meta } from '/imports/api/links/collections.js';
import { Comments } from '/imports/api/links/collections.js';
import { SchemasTours } from '/imports/api/links/collections.js';
import { Schemas } from '/imports/api/links/collections.js';
window.Meta = Meta;

//import { hooksAddImg } from '/imports/api/hooks.js';
import { hooksAddIcon } from '/imports/api/hooks.js';
import { hooksEditScene } from '/imports/api/hooks.js';
import { hooksEditPortal } from '/imports/api/hooks.js';
import { hooksPublishPano } from '/imports/api/hooks.js';
AutoForm.addHooks('editSceneForm', hooksEditScene);
AutoForm.addHooks('editPortalForm', hooksEditPortal);
AutoForm.addHooks('insertPanoTourForm', hooksPublishPano);
//AutoForm.addHooks('editTourForm', hooksAddIcon);
AutoForm.debug();

/* import { Slider } from 'bootstrap-slider';
import 'bootstrap-slider';  */
//import '/imports/external/bootstrap-slider.js';

window.Images = Images;
window.Tours = Tours;
/* window.SchemasImages = SchemasImages; */
window.SchemasTours = SchemasTours;
window.Tours.attachSchema(SchemasTours);

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

function resizeImage(url, width, height, callback) {
    var sourceImage = new Image();

    sourceImage.onload = function() {
        // Create a canvas with the desired dimensions
        var canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;

        // Scale and draw the source image to the canvas
        canvas.getContext("2d").drawImage(sourceImage, 0, 0, width, height);

        // Convert the canvas to a data URL in PNG format
        callback(canvas.toDataURL());
    }

    sourceImage.src = url;
}

Template.signinModal.onCreated( () => {
	AccountsTemplates.avoidRedirect = true;
});

Template.signinModal.onDestroyed( () => {
	AccountsTemplates.avoidRedirect = false;
});

Template.deletetourModal.events({
	'click .delete' ( e, t) {
		console.log('deletetourModal', this);
		Meteor.call('tour.remove', {_id:  this._id});
	},
});

Template.deletesceneModal.events({
	'click .delete' ( e, t) {
		console.log('deletesceneModal', this);
		Meteor.call('scene.remove', {_id: this._id});
/* 		Tours.update(this.tour_id, {$pull: {scene_id: this._id}});
		Tours.update(this.tour_id, {$pull:{panorama_id: this.panorama_id[0]}});
		Scenes.remove(this._id);
		Images.update(this.panorama_id[0],{$pull:{scene_id: this._id}}); */
/* 		Meteor.setTimeout(function(){
			slyInit('#slyframe','deletesceneModal');
		},100); */
	},
});

Template.tourDetailsModal.helpers({
	iconImg(){
		return this.icon.url.replace(/upload/, 'upload/'+Meteor.settings.public.CLOUDINARY_THUMB);	
	},	
});

Template.editSceneModal.onCreated( () => {
	var t = Template.instance();
	t.north = new ReactiveVar();
});
Template.editSceneModal.onRendered( () => {
	if (typeof(viewer) != 'object')
		return;
	var t = Template.instance();
//	var north;
	var init, north = 0;
	console.log('infspots step 0: ', viewer.panorama.children.length, viewer.panorama.children);

	const portalslength = viewer.panorama.children.length;
	while (viewer.panorama.children.length > 0){
		viewer.panorama.children[0].dispose();
		console.log('infspots: ', viewer.panorama.children.length, viewer.panorama.children);
	}
					
	$('#north').slider({
		formatter: function(value) {
			var _y, norm_y, fin_y ;
			if (typeof(viewer) == 'object') {
				//norm_y = 
				if (!Scenes.findOne(Session.get('currScene')))
					north = 0;
				else if (!init)
					north = Scenes.findOne(Session.get('currScene')).north;				
				//viewer.panorama.rotateY(-viewer.panorama.rotation._y);
//				+ THREE.Math.degToRad(value));
				if (viewer.panorama.rotation._x == 0)
					_y = THREE.Math.radToDeg(viewer.panorama.rotation._y);
				else
					_y = 180 - THREE.Math.radToDeg(viewer.panorama.rotation._y);

				//fin_y = Math.floor(- _y + value - north);
				fin_y = Math.floor(value - _y - north);
				if (init)
					viewer.panorama.rotateY(THREE.Math.degToRad(fin_y));
				else {
					//viewer.panorama.rotateY(THREE.Math.degToRad(fin_y));
					init = true;
					viewer.camera.position.set(0,0,1);
/* 					for (let portal of viewer.panorama.children) {
						portal.dispose();
					} */

				}
				//if (Scenes.findOne(Session.get('currScene')).north != fin_y) Scenes.update(Session.get('currScene'),{$set:{north: -value}});
				t.north.set(value);
			}
			console.log('slider', north, value, Scenes.findOne(Session.get('currScene')) );
			viewer.panorama.shift = THREE.Math.radToDeg(viewer.panorama.rotation._y);
			return 'Current value: ' + value;
		}
	});

	//window.northSlider = northSlider;

});
Template.editSceneModal.onDestroyed( () => {
	if (typeof(viewer) != 'object')
		return;
	var t = Template.instance();
/* 	for (let portal of viewer.panorama.children) {
		portal.dispose();
	} */
	var north =  parseInt(t.north.get());
	var updated = Scenes.update(Session.get('currScene'),{$set:{north: north, northused: true}});
	var scene = Scenes.findOne(Session.get('currScene'));
	console.log('editSceneModal updated north', north, t.north.get(), updated, viewer.panorama.children, viewer.panorama.children.length );
	viewer.panorama.north = north;
	//populatePortals(viewer.panorama);
	//viewer.panorama.dispose();
	//populateScene(scene);
	console.log('editSceneModal.onDestroyed', viewer.scene.children, Session.get('currScene'));
	_.findWhere(viewer.scene.children, {_id:Session.get('currScene')}).load();
	viewer.setPanorama(_.findWhere(viewer.scene.children, {_id:Session.get('currScene')}));
});
Template.editSceneModal.helpers({
	northneg(){
		return this.north || 0;
	},
	ifViewer(){
		return typeof(viewer) == 'object';
	}
});
Template.editSceneModal.events({
	'change'(e,t){
		console.log('editSceneModal change', e.target.value, this);		
	},
	'click .delete' ( e, t) {
		console.log('deletesceneModal', this);
		Tours.update(this.tour_id, {$pull: {scene_id: this._id}});
		Tours.update(this.tour_id, {$pull:{panorama_id: this.panorama_id[0]}});
		Scenes.remove(this._id);
		Images.update(this.panorama_id[0],{$pull:{scene_id: this._id}});
/* 		Meteor.setTimeout(function(){
			slyInit('#slyframe','editSceneModal');
		},100); */
	},
	'click .updateForm'(e){
		$("#editSceneForm").submit();
		e.preventDefault();
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	}

});

Template.editPortalModal2.events({
	'click .delete'(e,t){
		console.log('remove', e.target.id, this);
		Scenes.update({_id: this._id},{$pull:{portal:{next_scene: e.target.id}}});
		//viewer.panorama.children[0].dispose()
		for (i=0;i<viewer.panorama.children.length;i++){
			if (viewer.panorama.children[i].next_scene == e.target.id)
				viewer.panorama.children[i].dispose();
		}
		Modal.hide();
	},
	'click .submit'(e){
		$("#editPortalForm").submit();
		e.preventDefault();
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	}
});

Template.hotspotModal.onCreated( () => {
	AutoForm.debug(); 
	Meteor.subscribe('images');
	Meteor.subscribe('people');
	Meteor.subscribe('test');
	Template.instance().uploadWay = new ReactiveVar( false );
  Template.instance().uploading = new ReactiveVar( false );
	Template.instance().tour = new ReactiveVar( FlowRouter.current().queryParams.tour );
	Template.instance().tourData = new ReactiveVar( false );
	Template.instance().error = new ReactiveVar( false );
	Template.instance().editTourText = new ReactiveVar( false );
	
});
Template.hotspotModal.onRendered( () => {
	$( "#hotspot" ).draggable();
});
Template.hotspotModal.helpers({
	tour(){
		return Tours.findOne({id: this.tourId, 'panorama.id': this.panoId});
	},
	image(){
		var tour = Tours.findOne({id: this.tourId, 'panorama.id': this.panoId});
		console.log("hotspotModal img 1", this, tour);
/* 		if (tour)
			return; */
		var panorama = _.where(tour.panorama, {id: this.panoId});
		console.log("hotspotModal img 2", this, panorama);
/* 		if (!panorama)
			return; */
		var image = tour.cloudPath + 'c_fill,w_1024/' + tour.cloudFolder + panorama[0].cloud;
		console.log("hotspotModal img fin", image, this, panorama);
		return image
	},
	editTourText(){
		return Template.instance().editTourText.get();
	},
	ToursColl() {
		return Tours;
	},
	ToursCollSch() {
		console.log('schemas', SchemasTours, Schemas);
		return SchemasTours;
	},
	ImagesColl() {
		return Images;
	},
	TestColl() {
		return Test;
	},
  uploading() {
    return Template.instance().uploading.get();
  },
  tour() {
		var tour = Tours.findOne({id:Template.instance().tour.get()});
		//console.log('editTour tour', tour, SchemasTours, Template.instance().tour.get());
		return tour;
  },
  tourData() {
		var tour = Tours.findOne({id:Template.instance().tour.get()});
		console.log('uploaded tour', tour, Template.instance().tourData.get());
		return Template.instance().tourData.get();
	}
});
Template.hotspotModal.events({
	'click #editTourText' ( event, template ) {
		console.log('uploadWay', event.target.checked);
		template.editTourText.set(!template.editTourText.get());
	},
  'drag #hotspot'(event) {	
    if (event.drag.type === 'dragstart') {
      console.log('You start dragging!')
    } else if (event.drag.type === 'dragend') {
      console.log('You stopped dragging!', event, event.drag)
/*     } else if (event.drag.type === 'dragging') {
      console.log('You are dragging!') */
    }	
  },
/* 	'submit '(e){
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	} */
	'click .off'(e){
		$("#editSceneForm").submit();
		e.preventDefault();
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	}
});

Template.titleModal.onCreated( () => {
	AutoForm.debug(); 
	Template.instance().tour = new ReactiveVar( FlowRouter.current().queryParams.tour );
	Template.instance().tour_id = new ReactiveVar( Tours.findOne({id:FlowRouter.current().queryParams.tour})._id );
});
Template.titleModal.onRendered( () => {

});
Template.titleModal.helpers({
	titles(){
		var titles;
		var titles = Titles.findOne({tourId: this.tourId, file: this.file});
		console.log('titleModal titles', this, titles);
		return titles;
	},
});
Template.titleModal.events({
	'change .updateTitle' (e,t){
		console.log('updateTitle', e.target.id, e.target.value, this, t.tour.get(), t.tour_id.get());
		data = {};
		data.tourId = t.tour.get();
		data.panoId = e.target.id;
		data.text = e.target.value
		Meteor.call('titles.update',data);
		if (Titles.findOne({id: data.tourId, strings: {$elemMatch:{id: data.panoId, text: data.text}}}));
			Bert.alert({
				title: 'Title was updated',
				message: e.target.id + ': ' + e.target.value,
				type: 'info',
				style: 'growl-top-right',
			});
	}
/* 	'click #editTourText' ( event, template ) {
		console.log('uploadWay', event.target.checked);
		template.editTourText.set(!template.editTourText.get());
	},	 */
});

Template.viewpanoModal.onCreated( () => {
	let t = Template.instance();
	console.log('viewpanoModal oncreatrd template inst', t);
	t.error = new ReactiveVar();
	t.subscribe('meta', {image_id: t.data._id, caller: 'viewpanoModal'});
	t.subscribe('tours',{panorama_id: t.data._id});
	//Meteor.subscribe('meta');
});
Template.viewpanoModal.onRendered( () => {
	let t = Template.instance();
	console.log('viewpanoModal onRendered template inst', t);
	t.autorun(function(){
		var data = Meta.findOne({image_id:t.data._id});
		if (data && data.address && !data.sync)
		Meteor.call('image.group', {image_id: t.data._id});
	});
	//Meteor.subscribe('meta');
});
Template.viewpanoModal.helpers({
	dataImg(){
		var url = this.url.replace(/upload/, 'upload/' + Meteor.settings.public.CLOUDINARY_PREVIEW);
		return url;
	},
	meta(){
		var data = Meta.findOne({image_id:this._id});

		console.log('getting meta', this, data);
		return data;
	},
	formattedAddress(){
		var self = this;
		if (self.address && self.address.length)
			return self.address[0].formattedAddress;
	},
	insideTour(){
		return (FlowRouter.current().route.name == 'edit');
	},
	usedIn(){
		return _.pluck(Tours.find({panorama_id: this._id},{fields: {title:1}}).fetch(),'_id');
	},
	tourTitle(){
		//console.log('viewpanoModal tourTitle', this, this.valueOf()); 
		return Tours.findOne(this.valueOf());
	},
	error(){
		let t = Template.instance();
		Meteor.setTimeout(function(){
			$(".modalPano").each(function(){ 
				console.log('checking pano size', this.id, this.alt, this.src, this.naturalHeight, this.naturalWidth);
				if ( this.naturalHeight!=0 && this.naturalWidth / this.naturalHeight != 2 ) {
					console.warn('wrong dims', this.id, this.src, this.naturalHeight, this.naturalWidth);
					$('#panoImage').addClass('shadowred');
					//Bert.alert('Wrong size of file ' + this.alt, 'warning');
					t.error.set('We can process only images with dimensions 2:1, please check <a href="/faq">faq</a>');
				} else {
					$('#panoImage').removeClass('shadowred');
					//t.error.set('We can process only images with dimensions 2:1');
				}
			});
		},1000);
		return t.error.get();

	},
});
Template.viewpanoModal.events({
	'click .addScene' (e,t){
		console.log('viewpanoModal', this);
		addingScene({panorama: this._id});
		Modal.hide();
/* 		Meteor.setTimeout(function(){
			slyFrame.destroy();
			slyInit('#slyframe','viewpanoModal');
			slyFrame.reload();		
			slyFrame.toEnd();		
		},1000); */
	},
	'click .hideModal'(){
		Modal.hide();
	}
});

Template.editTagsModal.onCreated( () => {
	let t = Template.instance();
	console.log('editTagsModal oncreatrd template inst', t.data, t);
	t.error = new ReactiveVar();
	//Meteor.subscribe('meta');
});
Template.editTagsModal.onRendered( () => {
	let t = Template.instance();
	console.log('editTagsModal onRendered template inst', t.data);
	//Meteor.subscribe('meta');
});
Template.editTagsModal.helpers({
	schema(){
		return Schemas.CommonTags;		
	},
	doc(){
		doc = {};
		doc.images = this.images;
		var data = _.pluck(Images.find({_id: {$in:  this.images}}).fetch(), 'tags');
		doc.comtags = _.intersection.apply(_, data);
		doc.tags = doc.comtags;
		console.log('common 1', this, data, doc);
		return doc;
	},
	comtags(){
		console.log('common 0', this.images, this);
		var data = _.pluck(Images.find({_id: {$in:  this.images}}).fetch(), 'tags');
		var commonTags = _.intersection(data[0], data[1]);
		console.log('common 1', this, data, commonTags);
		return commonTags;
	},
	tags(){
		console.log('received images 0', this.images, this);
		var data = _.pluck(Images.find({_id: {$in:  this.images}}).fetch(), 'tags');
		var commonTags = _.intersection(data[0], data[1]);
		console.log('received images 1', this, data, commonTags);
		return commonTags;
	},
	error(){
		let t = Template.instance();
		return t.error.get();

	},
});
Template.editTagsModal.events({
	'click .addScene' (e,t){
		console.log('viewpanoModal', this);
		addingScene({panorama: this._id});
		Modal.hide();
/* 		Meteor.setTimeout(function(){
			slyFrame.destroy();
			slyInit('#slyframe','editTagsModal');
			//slyFrame.reload();		
			slyFrame.toEnd();		
		},1000); */
	},
	'click .hideModal'(){
		Modal.hide();
	}
});

Template.publishpanoModal.onCreated( () => {
	let t = Template.instance();
	console.log('publishpanoModal oncreatrd template inst', t);
	t.error = new ReactiveVar();
	Meteor.subscribe('meta', {image_id: t.data._id, caller: 'viewpanoModal'});
	//Meteor.subscribe('meta');
});
Template.publishpanoModal.onRendered( () => {
	let t = Template.instance();
	console.log('publishpanoModal onRendered template inst', t);
	t.autorun(function(){
		var data = Meta.findOne({image_id:t.data._id});
		//if (data && data.address && !data.sync) Meteor.call('image.group', {image_id: t.data._id});
	});
	Session.set('formInsert');
	//Meteor.subscribe('meta');
});
Template.publishpanoModal.helpers({
	doc(){
		var meta = Meta.findOne({image_id:this._id});
		var doc = this;
		doc.id = doc.file;
		doc.icon = {
			url: doc.url.replace(/upload/, 'upload/' + Meteor.settings.public.CLOUDINARY_PREVIEW)
		};
		doc.panorama_id = [this._id];
		if (meta && meta.address && meta.address.length){
			doc.location = {};
			if ( meta.address && meta.address.length)
				doc.location.address = meta.address[0].formattedAddress;
			if (meta.gps)
				doc.location.gps = meta.gps;
		}
		console.log('publishpanoModal doc', doc);
		return doc;
	},
	// meta(){
		// var data = Meta.findOne({image_id:this._id});
		// console.log('publishpanoModal getting meta', this, data);
		// return data;
	// },
	// formattedAddress(){
		// var self = this;
		// if (self.address && self.address.length)
			// return self.address[0].formattedAddress;
	// },
	dataImg(){
		var url = this.url.replace(/upload/, 'upload/' + Meteor.settings.public.CLOUDINARY_PREVIEW);
		return url;
	},
	panorama_id(){
		return [this._id];
	},
	error(){
		let t = Template.instance();
		Meteor.setTimeout(function(){
			$(".modalPano").each(function(){ 
				console.log('checking pano size', this.id, this.alt, this.src, this.naturalHeight, this.naturalWidth);
				if ( this.naturalHeight!=0 && this.naturalWidth / this.naturalHeight != 2 ) {
					console.warn('wrong dims', this.id, this.src, this.naturalHeight, this.naturalWidth);
					$('#panoImage').addClass('shadowred');
					//Bert.alert('Wrong size of file ' + this.alt, 'warning');
					t.error.set('We can process only images with dimensions 2:1, please check <a href="/faq">faq</a>');
				} else {
					$('#panoImage').removeClass('shadowred');
					//t.error.set('We can process only images with dimensions 2:1');
				}
			});
		},1000);
		return t.error.get();
	},
	added(){
		return Session.get('formInsert');
	},
	debug(){
		console.log('publishpanoModal debug', this);
	}
});
Template.publishpanoModal.events({
	'submit'(e,t){
		e.preventDefault();
	},
	'click .publishTour' (e,t){
		console.log('publishpanoModal', this);

	},
	'click .hideModal'(){
		Session.set('activeSet', true);
		Modal.hide();
	}
});

Template.screenshotModal.onCreated( () => {
	let t = Template.instance();
	t.resultSet =	new ReactiveVar();
	t.deleteIcon =	new ReactiveVar();
	t.error = new ReactiveVar();
	Meteor.subscribe('meta', {image_id: t.data._id, caller: 'viewpanoModal'});
	console.log('viewpanoModal oncreatrd template inst', t);
	t.autorun(function(){
		if (t.deleteIcon.get()) {
			Meteor.call('cloud.remove.icon', t.deleteIcon.get());
			t.deleteIcon.set()
		}
	});
	//Meteor.subscribe('meta');
});
Template.screenshotModal.helpers({
	dataUri(){
		var dataUri = document.getElementById('panolens-canvas').toDataURL('image/png', 0.5);
		var blob = dataURItoBlob(dataUri);
		return blob;
	},
	file(){	
		var dataUri = document.getElementById('panolens-canvas').toDataURL('image/png', 0.5);
		var blob = dataURItoBlob(dataUri);
		//return dataUri;		
		//var png = ReImg.fromCanvas(document.getElementsByClassName('panolens-canvas')[0]).toPng();
		
		var href = window.URL.createObjectURL(blob);
		console.log('file', blob, href);
		return href;
	},
	href(){
		//var img = this.toDataURL();
		console.log('screenshotModal 0', this);
		var filename = this.title + '.png';
		//var file =  new File([this.blob], filename);
		//var file = blobToFile(this.blob, filename);
		//var file = new File([myBlob], "name");
		//var blob = new Blob([this.blob], {type: "application/octet-stream"});
		//var url = window.URL.createObjectURL(file);
		var myBlob = dataURItoBlob(this.blob);
		myBlob.name = "image.jpeg";
		console.log('screenshotModal 1', filename);
		//return this.blob.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
		//return this.blob.pngStream();
		//return myBlob;
		//return this.blob.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
		//return url;
	},
	result(){
		let t = Template.instance();
		return t.resultSet.get();
	}
});
Template.screenshotModal.events({
	'click .download'(e,t){
		e.preventDefault();
		var canvas = $('#panolens-canvas');
		console.log('canvas', canvas);
		//ReImg.fromCanvas(canvas).downloadPng();
	},	
	'click .forIcon'(e,t){
		e.preventDefault();
		var self = this;
		var options = {}, canvas, env, elUpload, dataUri, blob, res1, res2, env;
		$('.forIcon').find('i').addClass('fa-spinner fa-spin').removeClass('fa-wpexplorer');
		canvas = $('#screen-canvas');
		console.log('forIcon', canvas, this, e);
		dataUri = document.getElementById('panolens-canvas').toDataURL('image/png', 0.5);
		//var blob = dataURItoBlob(dataUri);
		//var filesList = [blob];
		options.upload_preset = Meteor.settings.public.CLOUDINARY_PRESET;
		options.folder = 'icon'
		env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app|dev/);
		if (env)
			env = env[0];
		else
			env = 'dev';
		elUpload = $('.image_upload');
		//var data = canvas.toDataURL('image/png');
		
		options.tags = env + ',' + Meteor.userId() + ',' + Meteor.user().username + ', icon';
		Meteor.call('cloud.sign', options, function (err, res) {
			if (err) {
				return console.log(err);
			}
			if (Session.get('debug')) console.log('afCloudinarySign', self, 'res:', res, 'options:', options, '\n\n\n');

			res1 = elUpload.cloudinary_fileupload({
				formData: res,
			// plus any other options, eg. maxFileSize
			});	

/* 			res1 = canvas.fileupload('send', {
				formData: res,
				autoUpload: true,
				files: filesList,
				file: blob,
				url: 'https://api.cloudinary.com/v1_1/ufg/auto/upload',
				function (e,r){
					console.log('cloudinary_fileupload 1', e, r);
				}
			// plus any other options, eg. maxFileSize
			});	 */
			
			elUpload.fileupload('option', 'formData').file = dataUri;
			res2 = elUpload.fileupload('send', { files: [ dataUri ] })
			
			elUpload
				.bind('fileuploadstart', function(e, data) {
					if (Session.get('debug')) console.log('fileuploadstart:', e,  data, '\n\n\n');
				})
				.bind('cloudinarydone', function(e, data) {
					if (Session.get('debug')) console.log('cloudinarydone:', e, data, '\n\n\n');
					if (data && data.result) {
						Tours.update(self._id,{$set:{'icon.url': data.result.secure_url}});
						t.resultSet.set('tour icon was updated');
						if (self.icon && self.icon.url) t.deleteIcon.set(self.icon);
						Bert.alert(t.resultSet.get(), 'success');
						$('.forIcon').find('i').removeClass('fa-spinner fa-spin').addClass('fa fa-check-circle-o');
					} else {
						if (Session.get('debug')) console.warn('cloudinarydone failed:', e, data, '\n\n\n');
						$('.forIcon').find('i').removeClass('fa-spinner fa-spin').addClass('fa-wpexplorer red');
					}
				});

			console.log('res1, res2', res1, res2);
		});
	},
	'click .nothere'(){
		e.preventDefault();
		//var img = this.toDataURL();
		var link = document.createElement('a');
		var canvas = document.getElementById("screen-canvas");

		var imageData = canvas.getContext('2d').createImageData(512, 256);
		imageData.data.set(myData);


		console.log('link blob', link);
		
		link.download = this.title + '.png';
		document.body.appendChild(link);
		link.click();     
	
		
		//window.saveAs(file, "pretty image.png");
		// draw to canvas...
		// canvas.toBlob(function(blob) {
			// window.saveAs(blob, "pretty image.png");
		// });
		
/* 		var filename = this.title + '.png';
		console.log('screenshotModal 1', filename);
		//var file = blobToFile(this.blob, filename);
		var file = new File([this.blob], "name");
		var blob = new Blob([this.blob],{ "type" : "image/png"});
		console.log('click .download 0', this, file);
		//window.saveAs(this.blob, filename);
		window.open(this.blob.toDataURL()); */
	},
	'click .useIcon' (e,t){
		Files.insert({
			file: this.blob,
			isBase64: true, // <— Mandatory
			fileName: this.title + '.png' // <— Mandatory
		});		
	}
});

Template.shareModal.onCreated( () => {
	let t = Template.instance();
	t.sharingService = new ReactiveVar(0);
	t.selectPano = new ReactiveVar(0);
	t.north = new ReactiveVar(0);
	t.showEmbed = new ReactiveVar();
	t.sharingRes = new ReactiveVar();
	t.showText = new ReactiveVar();
	t.error = new ReactiveVar();
	t.subscribe('images',{tour_id: t.data._id});
});
Template.shareModal.onRendered( () => {
	let t = Template.instance();
	console.log('shareModal onrendered', t.data);
	t.autorun(()=>{
		console.log('t.sharingService.get', t.sharingService.get());
		if (!t.sharingService.get()) return;
		Meteor.setTimeout(()=>{
			$('#north').slider({
				formatter: function(value) {
					//console.log('slider', value );
					t.north.set(value);
				}
			});
		},200);
	});
	
});
Template.shareModal.helpers({
	sharingService(){
		let t = Template.instance();
		return t.sharingService.get();
	},	
	north(){
		let t = Template.instance();
		return t.north.get();
	},
	lineposition(){
		let t = Template.instance();
		var lineposition = t.north.get()/18*5 + 50;
		//console.log('lineposition', lineposition);
		return lineposition;		
	},
	url(){
		var url = window.location.origin + '/tour/' + this._id;
		return url;
	},
	embed(){
		if (!Template.instance().showEmbed.get())
			return;
		var url = window.location.origin + '/embed/' + this._id;
		var embed = '<iframe width="1280" height="720" src="' + url +'" frameborder="0" allowfullscreen></iframe>';
		return embed;
	},
	iconUrl(){
		console.log('share icon', this);
		if (this.icon && this.icon.url)
			return this.icon.url.replace(/upload/, 'upload/'+Meteor.settings.public.CLOUDINARY_PREVIEW);
		return '/img/camera.png';
	},	
	dataImg(){
		let t = Template.instance();
		var imgurl;
		if (t.sharingService.get() == 'facebook') 
			imgurl = Images.findOne(this.panorama_id[t.selectPano.get()]).url;
		else if (this.icon)
			imgurl = this.icon.url;
		else
			imgurl = Images.findOne(this.panorama_id[t.selectPano.get()]).url;
		
		return imgurl.replace(/upload/, 'upload/'+Meteor.settings.public.CLOUDINARY_PREVIEW);
		//return '/img/camera.png';
	},
	facebook(){
		var data = Meteor.user().services.facebook;
		console.log('facebook', data);
		return data;
	},
	options(){
		var user = Meteor.user();
		if (!user || !user.services || !user.services.facebook || !user.services.facebook.acoounts)
			return console.warn('no user accounts', user.services.facebook);
		var options = user.services.facebook.accounts;

    var map = _.map(options, function (c) {
      return {label: c.name, value: c.id};
    });
		map.push({label: 'Me', value: 'me'});
		console.log('options', options, map);
		return map;
	},
	schema(){
		return Schemas.SettingsFacebook;
	},
	showText(){
		let t = Template.instance();
		return t.showText.get();
	},
	sharingRes(){
		let t = Template.instance();
		return t.sharingRes.get();
	},
	error(){
		let t = Template.instance();
		return t.error.get();
	}
});
Template.shareModal.events({
	'click .arrow-left'(e,t){
		e.stopPropagation();
		var length = this.panorama_id.length;
		var index = t.selectPano.get() - 1 || 0;
		if (index < 0)
			index = length - 1;
		t.selectPano.set(index);
		console.log('clicked arrow', t.selectPano.get());
	},		
	'click .arrow-right'(e,t){
		e.stopPropagation();
		var length = this.panorama_id.length;
		var index = t.selectPano.get() + 1;
		if (index > length - 1)
			index = 0;
		t.selectPano.set(index);
		console.log('clicked arrow', t.selectPano.get());
	},	
	'click .selectable' (e,t){
		SelectText(e.target.id);
		$('.selectable').removeClass('selected');
		$('#' + e.target.id).addClass('selected');
	},
	'click .embed'(e,t){
		t.sharingRes.set({info: '	copy that embed code'});
		t.showEmbed.set(!t.showEmbed.get());
		t.showText.set();
	},	
  'click .connectfacebook'(e,t) {
		t.error.set();
		Meteor.linkWithFacebook({requestPermissions: ['user_friends', 'email', 'publish_actions']}, (err, r)=> {
			console.log('linkWithFacebook', err, r);
			if (err)
				t.error.set(err.error);
			else
				t.error.set();
		});
  },
	'click .shareIt'(e,t){
		 t.sharingService.set();
		if (!Meteor.userId()) {
			FlowRouter.go('/sign-in');
			return Modal.hide();
		}
		analytics.track('commentsLikes', {
			category: 'Account',
			label: 'share',
			eventName: "share",
			value: FlowRouter.getRouteName(),
		});		
		var domain = window.location.origin.replace(/stg/,'www');
		var href = domain + '/tour/' + this._id;
		var user = Meteor.user();
		console.log('clicked shareIt', e.currentTarget.id, 'e:', e, 'user:', user, 'this:', this);
		t.sharingRes.set();
		t.showEmbed.set();
				
		if (e.currentTarget.id == 'facebook'){
			Meteor.call('social.facebook.accounts');
			t.sharingService.set('facebook');
			if (!user.services.facebook)
				Meteor.linkWithFacebook({requestPermissions: ['user_friends', 'email', 'publish_actions']}, (err, r)=> {
					console.log('linkWithFacebook', err, r);
					if (err)
						t.error.set(err.error);
					else
						t.error.set();
				});
			$('.facebook').removeClass('shareIt');
			t.showText.set('fbtext');
		} else if (e.currentTarget.id == 'twitter'){
			$('.facebook').addClass('shareIt');
			t.showText.set('twtext');
		} else {
			$('.facebook').addClass('shareIt');
			t.showText.set();
			console.log('clicked shareIt 2', e, this);
			t.sharingRes.set({info: 'this feature is coming soon'});
			Bert.alert('this feature is coming soon', 'info');
		}
	},
	'submit'(e,t){
		e.preventDefault();
		
		if (!e.target.id.match(/fbtext|twtext/))
			return console.log('submit out', e.target, e, this);
		
		$('.shareSubmit').fadeOut();
		
		t.sharingRes.set({info:'Please wait, we are sharing this tour'});
		var self = this;
		self.text = $('#showText').val();
		self.north = t.north.get();
		self.index = t.selectPano.get();
		console.log('submit', e, this);
		if (e.target.id == 'fbtext'){
			self.page_id = $('#shareForm').find('select[name="account"] option:selected').val();
			self.privacy = $('#shareForm').find('select[name="privacy"] option:selected').text();
			Meteor.call('social.facebook.post', self,  (err, res) => {
				console.log('social.facebook.post err,res:', err, res);
				if (err) {
					var msg;
					if (err.details)
						msg = err.details.error_user_msg || err.details.message;
					else
						msg = err.reason;
					Bert.alert('FB sharing failed' + msg, 'dangerous');
					t.sharingRes.set({info: msg});
				} else {
					t.showText.set();
					Bert.alert('that tour was shared on FB', 'info');
					res.service = 'Facebook';
					res.link = 'http://www.facebook.com/photo.php?fbid=' + res.id;
					t.sharingRes.set(res);
					$('.facebook').removeClass('pointer').addClass('sharedDone');
				}
			});			
		} else if (e.target.id == 'twtext')
			Meteor.call('social.twitter.post', self,  (err, res) => {
				console.log('social.twitter.post err,res:', err, res);
				if (err) {
					var msg = err.details.error_user_msg || err.details.message;
					Bert.alert('twitter sharing failed' + msg, 'dangerous');
					t.sharingRes.set({info: msg});
				} else {
					t.showText.set();
					Bert.alert('that tour was shared on twitter', 'info');
					res.service = 'twitter';
					res.link = 'https://twitter.com/' + res.user.screen_name + '/status/' + res.id_str;
					t.sharingRes.set(res);
					$('.twitter').removeClass('pointer').addClass('sharedDone');
				}
			});	
	},
/* 	'click .soon'(){
		Bert.alert('this feature is coming soon', 'info');
	}, */
	'click .sharedDone'(){
		Bert.alert('This link was shared already', 'info');
	},
});

Template.viewedModal.helpers({
	tour(){
		var tour_id = this.tour_id || this._id;
		return Tours.findOne(tour_id);
	},	
});

Template.embedModal.onCreated( () => {
	Template.instance().showEmbed = new ReactiveVar(  );
});
Template.embedModal.helpers({
	embed(){
		var url = window.location.origin + '/embed/' + this._id;
		var embed = '<iframe width="1280" height="720" src="' + url +'" frameborder="0" allowfullscreen></iframe>';
		return embed;
	},
	iconUrl(){
		console.log('share icon', this);
		if (this.icon && this.icon.url)
			return this.icon.url;
		return '/img/camera.png';
	}
});
Template.embedModal.events({
	'click .selectable' (e,t){
		SelectText(e.target.id);
		$('.selectable').removeClass('selected');
		$('#' + e.target.id).addClass('selected');
	},
	'click .embed'(e,t){
		t.showEmbed.set(!t.showEmbed.get());
	},
	'click .soonBert'(){
		Bert.alert('this feature is coming soon', 'info');
	}
});

Template.prouserModal.helpers({
	upload(){
		return this.max/1024/1024;
	},
	plan(){
		return this.type.toUpperCase();
	},
	exp(){
		return this.expire.toLocaleDateString("en-US");
	}
});
Template.prouserModal.events({
	'click .submit'(e){
		$("#prousersForm").submit();
		e.preventDefault();
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	}
});

Template.serverofflineModal.helpers({
	status: function(){
		var status = Meteor.status();
		if ((status.status == 'connected'))
			status.checked = 'checked';
		else
			status.checked = '';
		status.server = status.status;
		console.log('meteor status ',  status);
		return status;
	},
});
Template.serverofflineModal.events({
	'change .updateTitle' (e,t){
		console.log('updateTitle', e.target.id, e.target.value, this, t.tour.get(), t.tour_id.get());
		data = {};
		data.tourId = t.tour.get();
		data.panoId = e.target.id;
		data.text = e.target.value
		Meteor.call('titles.update',data);
		if (Titles.findOne({id: data.tourId, strings: {$elemMatch:{id: data.panoId, text: data.text}}}));
			Bert.alert({
				title: 'Title was updated',
				message: e.target.id + ': ' + e.target.value,
				type: 'info',
				style: 'growl-top-right',
			});
	}
/* 	'click #editTourText' ( event, template ) {
		console.log('uploadWay', event.target.checked);
		template.editTourText.set(!template.editTourText.get());
	},	 */
});

Template.inviteUserModal.events({
	'click .like'(e,t){
		e.preventDefault();
		analytics.track('commentsLikes', {
			category: 'Account',
			label: 'likeModal',
			eventName: "likeModal",
			value: FlowRouter.getRouteName(),
		});
		if (!Meteor.userId()) return FlowRouter.go('/user');
		
		
		// this.origin = window.location.origin;
		// if (this.liked && _.contains(this.liked, Meteor.userId()))
			// console.log('tour was liked, not sending to fb likes', this.liked, this.facebook);
		// else
			// Meteor.call('social.facebook.like', this,  (err, res) => {
				// console.log('social.facebook.like err,res:', err, res);
				// if (err) {
					// var msg = err.details.error_user_msg || err.details.message;
					// Bert.alert('FB like failed' + msg, 'dangerous');
				// } else if (res) {
					// Bert.alert('that tour was liked on FB', 'info');
				// }
			// });
		Meteor.call('tour.liked', {_id: FlowRouter.getParam('id')});	
		console.log('clicked like in Modal', FlowRouter.getParam('id'), e, this);
		Modal.hide();
	},
	'click .openComments'(e,t){
		FlowRouter.setQueryParams({'comments':true});
		//t.openComments.set(this);
		$('#commentsSide').removeClass('hidden slideOutRight').addClass('slideInRight');
		console.log('openComments in Modal', $(e.target).hasClass('openComments'), e.target.id, e.target, e, $('#commentsSide').hasClass('slideInRight'));
		Modal.hide();
	},
	'click a'(e,t){
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	},
});

Template.tryModal.onRendered( () => {
	const t = Template.instance();
	console.log('tryModal.onRendered', t.data);
	Meteor.setTimeout( () => {
		$('.animated').addClass('bounce');
	},1000);
	Meteor.setTimeout( () => {
		$('.animated').removeClass('bounce').addClass('tada');
	},2000);
});
Template.tryModal.events({
	'click .exploreVR'(e,t){
		Modal.hide();
		FlowRouter.go('/worldtours');
	},
	'click a'(e,t){
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	},
});



import { Push } from '/imports/api/links/collections.js';
import {getPermission} from '/imports/startup/client/push-client.js';
Template.notificationsModal.onCreated( () => {
	const t = Template.instance();
	t.details = new ReactiveVar(  );
});
Template.notificationsModal.onRendered( () => {
	const t = Template.instance();
	console.log('notificationsModal.onRendered', t.data, window.olark);
	if (window.olark) {
		//console.log('checking olark');
		window.olark('api.visitor.getDetails', (details)=>{
			t.details.set(details);
		})
	}
	Meteor.setTimeout( () => {
		$('.animated').addClass('bounce');
	},1000);
	Meteor.setTimeout( () => {
		$('.animated').removeClass('bounce').addClass('tada');
	},2000);
});
Template.notificationsModal.events({
	'click .enablePush'(e,t){
		//console.log('visitor details', t.details.get());
		getPermission(t.details.get());
		Modal.hide();
	},
	'click a'(e,t){
		Meteor.setTimeout(function(){
			Modal.hide();
		},500);
	},
});

Template.feedbackModal.onCreated(function () {
	
});
Template.feedbackModal.helpers({
	isCordova: function(){
		return Meteor.isCordova;
	},
	submitted: function(){
//		return Contact.findOne(Session.get('submitted'));
		return Session.get('submitted');
	},
	contactUser: function(){
		var user = Meteor.user();
		if (!user)
			return;
		user.userId = user._id;
		user.name = user.profile.name;
		if ((user.emails) && (user.emails[0]))
			user.email = user.emails[0].address;
		//console.log('feedback contactUser', user);
		return user;
	},
	env: function(){
		return Session.get('env');
	}
});

Template.sendMessageModal.helpers({
	schema(){
		return Schemas.SendMessage;
	},
	doc(){
		var doc = this;
		doc.recipientId = doc.value.messenger;
		console.log('sendMessageModal', doc);
		return doc;
	}
});
Template.sendMessageModal.events({
	'submit'(e,t){
		e.preventDefault();
		console.log('sendMessageModal', e, this);
	},
});

Template.sendPushModal.helpers({
	schema(){
		return Schemas.SendPush;
	},
	doc(){
		var doc = this;
		console.log('sendMessageModal', doc);
		doc.recipientId = doc._id;
		
		return doc;
	}
});
Template.sendPushModal.events({
	'submit'(e,t){
		e.preventDefault();
		console.log('sendMessageModal', e, this);
	},
});

