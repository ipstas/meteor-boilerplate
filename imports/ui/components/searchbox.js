import './searchbox.html';
import { UserIndex } from '/imports/startup/both/search.js';
import { TourIndex } from '/imports/startup/both/search.js';
import { TagsIndex } from '/imports/startup/both/search.js';

// On Client
Template.searchBox.helpers({  
	tourIndex: () => TourIndex,
  tagsIndex: () => TagsIndex,
	userIndex: () => UserIndex,
  //searchIndexes: () => [TourIndex, UserIndex],
  //searchIndexes: () => [UserIndex],
  searchIndexes: () => [UserIndex,TourIndex,TagsIndex],

  inputAttributes: () => {
    return {
      placeholder: 'user or tour search',
			class: 'form-control'
    }
  },
});