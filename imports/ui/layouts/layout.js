//import Raven from 'raven-js';

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
//import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { Random } from 'meteor/random';
//import { SubsManager } from 'meteor/meteorhacks:subs-manager' ;
//import { AutoForm } from 'meteor/aldeed:autoform';

import { Collections } from '/imports/api/links/collections.js';
import { Settings } from '/imports/api/links/collections.js';
import { Schemas } from '/imports/api/links/collections.js';

//PostSubs = new SubsManager();
window.Collections = Collections;

//import Leanplum from '/imports/external/leanplum.js';
import './layout.html';
import '../components/nav.js';
import '../components/footer.js';

BlazeLayout.setRoot('body');
//import { Push } from '/imports/api/links/collections.js';

//import '/imports/functions/page_scroll.js';
//import '/imports/external/grayscale.js';
//import {getPermission} from '/imports/startup/client/push-client.js';
//import {loadScripts} from '/imports/startup/client/ajaxscripts.js';
//import {olarkInit} from '/imports/startup/client/integration.js';
//import {cannyInit} from '/imports/startup/client/integration.js';
//import {processResponse} from '/imports/startup/client/watson.js';


function historyVirgo(t){
	var prevloc = window.document.location;
	if (FlowRouter.getRouteName() == 'tour')
		return;
	if (!Session.get('historyVirgo') || Session.get('historyVirgo').pathname != prevloc.pathname)
		Session.set('historyVirgo', prevloc);
	t.autorun(function(){
		if (FlowRouter.getRouteName() == 'tour' || Session.get('historyVirgo').pathname == FlowRouter.current().route.path)
			return console.log('historyVirgo same 0:', Session.get('historyVirgo').pathname, FlowRouter.current().route.path);
		prevloc = window.document.location;
		Session.set('historyVirgo', prevloc);
		if (Session.get('debug')) console.log('historyVirgo 1:', Session.get('historyVirgo').pathname, 'pathname:', prevloc.pathname, FlowRouter.current().route.path, 'session:', Session.get('historyVirgo'), 'prev:', prevloc );
	});
}

function setOlarkDetails(t){
	Meteor.setTimeout(()=>{
		if (window.olark) {
			window.olark('api.visitor.getDetails', (details)=>{
				Session.setPersistent('olarkdetails',details);
				console.log('push set olarkdetails', details);
			});
		} else if (!window.olark && !Session.get('olarkdetails'))
			Session.set('olarkdetails','no olark present');
	},12000);
}

// function firebasePermission(t){
	// Meteor.setTimeout(()=>{
		// Session.set('olarkdetails', 'test'); 
		// t.autorun(()=>{
			// if (Session.get('firebaseToken') || !Session.get('olarkdetails') || !subspush.ready() )
				// return console.warn('push layout checking permissions', Session.get('firebaseToken'), Session.get('olarkdetails'), subspush.ready());
			// var caller = 'layout';
			// console.log('push asking permissions with', Session.get('firebaseToken'), Session.get('olarkdetails'), subspush.ready());
			// getPermission(Session.get('olarkdetails'), caller);
		// });
	// },2500);
// }

function env(){
	var env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app|dev/);
	if (env)
		env = env[0];
	else
		env = 'dev';
	
	return env;
}

function pushOlark(){
	if (!Meteor.userId() && !Session.get('virgoUserId') && !Session.get('olarkdetails')) return;
	//if (!pushsubs.get()) return;
	
	var userId = Meteor.userId() || Session.get('virgoUserId').userId;
	var olarkdetails = Session.get('olarkdetails');

	var push = Push.findOne({userId:userId});
	if (!push)
		return;
	if (!olarkdetails || !olarkdetails.visitCount) {
		if (Session.get('debug')) console.warn('push no olarkdetails', olarkdetails) ;
		return; 
	}
	if (push && push.details && push.details.visitCount == olarkdetails.visitCount)  {
		if (Session.get('debug')) console.warn('push same olarkdetails', olarkdetails) ;
		return; 
	}
	Meteor.call('push.update', {_id: push._id, olarkdetails: olarkdetails});
	if (Session.get('debug')) console.log('push olark updated', olarkdetails);	
}

function setUserId(t){

/* 	if (!Session.get('referrer') && document.referrer && !document.referrer.match('360zipper.com')) {
		Session.setPersistent('referrer', {ref: document.referrer, link: FlowRouter.getQueryParam('link'), date: new Date()});
		console.log('setUserId referrer', Session.get('referrer'));
	}
	if (!Meteor.userId() && !Session.get('virgoUserId')) {
		Session.setPersistent('virgoUserId', {userId: Random.id(), username:'anonymous', date: new Date()});
		console.log('new user setUserId', Session.get('virgoUserId'), Session.get('referrer'));
	} else if (Meteor.userId() && Meteor.user() && Session.get('virgoUserId') && Session.get('virgoUserId').userId != Meteor.userId()) {
		Session.setPersistent('virgoUserId', {userId: Meteor.userId(), username: Meteor.user().username, date: new Date()});
		console.log('setUserId', Session.get('virgoUserId'), Session.get('referrer'));
	}  */
	if (!t)
		return console.warn('no template defined');
	else if (!Meteor.userId() && !Session.get('virgoUserId')) 
		return console.warn('no userId', Session.get('virgoUserId'));
	
	var userId = Meteor.userId() || Session.get('virgoUserId').userId;
	var subspush = Meteor.subscribe('push', {userId: userId});
	var olarkDetails;

		
/* 	if (!t.olarkdetails)
		t.olarkdetails = new ReactiveVar('test'); */
	
/* 		Tracker.autorun(()=>{
			var timeout = 30 * 60 * 1000;
			if (subspush.ready() && (!Session.get('pushAsked') || new Date() - Session.get('pushAsked') > timeout)) {	
				var push = Push.findOne({userId: userId});
				if (!push) {
					Modal.show('notificationsModal');
					Session.setPersistent('pushAsked', new Date());
				}
			}
		}); */
	//Meteor.call('user.visited');
}

function initFullH(){
	if (navigator.userAgent.match(/seo4ajax|prerender/)) return;
	if (window._fs_org) return;
	window['_fs_debug'] = false;
	window['_fs_host'] = 'www.fullstory.com';
	window['_fs_org'] = Meteor.settings.public.fullstory.fs_org;
	window['_fs_namespace'] = 'FS';	
	
	var env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app/);
	if (!env) return;
	
	(function(m,n,e,t,l,o,g,y){
			if (e in m && m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].'); return;}
			g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[];
			o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
			y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
			g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)};
			g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
			g.clearUserCookie=function(c,d,i){if(!c || document.cookie.match('fs_uid=[`;`]*`[`;`]*`[`;`]*`')){
			d=n.domain;while(1){n.cookie='fs_uid=;domain='+d+
			';path=/;expires='+new Date(0).toUTCString();i=d.indexOf('.');if(i<0)break;d=d.slice(i+1)}}};
	})(window,document,window['_fs_namespace'],'script','user');		
}

function kuollInit(){
	(function (w, k, t) {
		if (!window.Promise) {
			w[k] = w[k] || function () {};
			return;
		}
		w[k] = w[k] || function () {
			var a = arguments;
			return new Promise(function (y, n) {
				(w[k].q = w[k].q || []).push({a: a, d: {y: y, n: n}});
			});
		};
		var s = document.createElement(t),
				f = document.getElementsByTagName(t)[0];
		s.async = 1;
		s.src = 'https://cdn.kuoll.com/bootloader.js';
		f.parentNode.insertBefore(s, f);
	}(window, 'kuoll', 'script'));
	var userEmail;
	var user = Meteor.user();
	if (user &&  user.emails && user.emails[0])
		userEmail = user.emails[0].address;
	kuoll('startRecord', { // http://www.kuoll.com/js-doc/module-kuollAPI.html#.startRecord
			API_KEY: "K101DC4EF0N1LU",
			userId: Meteor.userId(),
			userEmail: userEmail,
			createIssueOn:	{"Error": true, "consoleError": true, "serverError": true}
	});	
}

function localytics(){
	var options = {
		appVersion: __meteor_runtime_config__.ROOT_URL,
		trackPageView: true
	};
	if (Meteor.userId())
		options.customerId = Meteor.userId();
	
	+function(l,y,t,i,c,s) {
			l['LocalyticsGlobal'] = i;
			l[i] = function() { (l[i].q = l[i].q || []).push(arguments) };
			l[i].t = +new Date;
			(s = y.createElement(t)).type = 'text/javascript';
			s.src = '//web.localytics.com/v3/localytics.js';
			(c = y.getElementsByTagName(t)[0]).parentNode.insertBefore(s, c);
			ll('init', 'ae39a67c8369f769bf52026-4467b9c2-3cdd-11e7-2329-00827c0501b4', options);
	}(window, document, 'script', 'll');
}

function segmentio(){
	if (!window.analytics) {
		!function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
		analytics.load("WxRtU0woR7SVk1yul4ee9wSESuX94a9E");
		analytics.page();
		}}();
	}
}

function leanplumInit(){
	$.getScript('/js/leanplum.js', function(script, textStatus, jqXHR){
		console.log('leanplum was loaded', textStatus, jqXHR);
		var env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app/);

		// Sample variables. This can be any JSON object.
		var variables = {
		 items: {
			 color: 'red',
			 size: 20,
			 showBadges: true
		 },
		 showAds: true
		};

		// We've inserted your Test API keys here for you :)
		if (!env) {
		 Leanplum.setAppIdForDevelopmentMode(Meteor.settings.public.leanplum.appId, Meteor.settings.public.leanplum.development);
		} else {
		 Leanplum.setAppIdForProductionMode(Meteor.settings.public.leanplum.appId, Meteor.settings.public.leanplum.production);
		}

		Leanplum.setVariables(variables);
		Leanplum.start(function(success) {
		 console.log('Leanplum start. Success: ', success, 'Variables', Leanplum.getVariables());
		});
	});
}

function heapInit(){
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("1714011256");
}

function rollbarInit(t){	
	if (env() == 'dev')
		return;
	var _rollbarConfig = {
			accessToken: Meteor.settings.public.rollbar.accessToken,
			verbose: true,
			ignoredMessages: ["(unknown): Script error."],
			captureUncaught: true,
			captureUnhandledRejections: true,
			payload: {
					environment: env
			}
	};
	
	// Rollbar Snippet
	!function(r){function o(e){if(n[e])return n[e].exports;var t=n[e]={exports:{},id:e,loaded:!1};return r[e].call(t.exports,t,t.exports,o),t.loaded=!0,t.exports}var n={};return o.m=r,o.c=n,o.p="",o(0)}([function(r,o,n){"use strict";var e=n(1),t=n(4);_rollbarConfig=_rollbarConfig||{},_rollbarConfig.rollbarJsUrl=_rollbarConfig.rollbarJsUrl||"https://cdnjs.cloudflare.com/ajax/libs/rollbar.js/2.1.0/rollbar.min.js",_rollbarConfig.async=void 0===_rollbarConfig.async||_rollbarConfig.async;var a=e.setupShim(window,_rollbarConfig),l=t(_rollbarConfig);window.rollbar=e.Rollbar,a.loadFull(window,document,!_rollbarConfig.async,_rollbarConfig,l)},function(r,o,n){"use strict";function e(r){return function(){try{return r.apply(this,arguments)}catch(r){try{console.error("[Rollbar]: Internal error",r)}catch(r){}}}}function t(r,o){this.options=r,this._rollbarOldOnError=null;var n=s++;this.shimId=function(){return n},window&&window._rollbarShims&&(window._rollbarShims[n]={handler:o,messages:[]})}function a(r,o){var n=o.globalAlias||"Rollbar";if("object"==typeof r[n])return r[n];r._rollbarShims={},r._rollbarWrappedError=null;var t=new d(o);return e(function(){return o.captureUncaught&&(t._rollbarOldOnError=r.onerror,i.captureUncaughtExceptions(r,t,!0),i.wrapGlobals(r,t,!0)),o.captureUnhandledRejections&&i.captureUnhandledRejections(r,t,!0),r[n]=t,t})()}function l(r){return e(function(){var o=this,n=Array.prototype.slice.call(arguments,0),e={shim:o,method:r,args:n,ts:new Date};window._rollbarShims[this.shimId()].messages.push(e)})}var i=n(2),s=0,c=n(3),p=function(r,o){return new t(r,o)},d=c.bind(null,p);t.prototype.loadFull=function(r,o,n,t,a){var l=function(){var o;if(void 0===r._rollbarDidLoad){o=new Error("rollbar.js did not load");for(var n,e,t,l,i=0;n=r._rollbarShims[i++];)for(n=n.messages||[];e=n.shift();)for(t=e.args||[],i=0;i<t.length;++i)if(l=t[i],"function"==typeof l){l(o);break}}"function"==typeof a&&a(o)},i=!1,s=o.createElement("script"),c=o.getElementsByTagName("script")[0],p=c.parentNode;s.crossOrigin="",s.src=t.rollbarJsUrl,n||(s.async=!0),s.onload=s.onreadystatechange=e(function(){if(!(i||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState)){s.onload=s.onreadystatechange=null;try{p.removeChild(s)}catch(r){}i=!0,l()}}),p.insertBefore(s,c)},t.prototype.wrap=function(r,o){try{var n;if(n="function"==typeof o?o:function(){return o||{}},"function"!=typeof r)return r;if(r._isWrap)return r;if(!r._wrapped&&(r._wrapped=function(){try{return r.apply(this,arguments)}catch(e){var o=e;throw"string"==typeof o&&(o=new String(o)),o._rollbarContext=n()||{},o._rollbarContext._wrappedSource=r.toString(),window._rollbarWrappedError=o,o}},r._wrapped._isWrap=!0,r.hasOwnProperty))for(var e in r)r.hasOwnProperty(e)&&(r._wrapped[e]=r[e]);return r._wrapped}catch(o){return r}};for(var u="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection".split(","),f=0;f<u.length;++f)t.prototype[u[f]]=l(u[f]);r.exports={setupShim:a,Rollbar:d}},function(r,o){"use strict";function n(r,o,n){if(r){var t;"function"==typeof o._rollbarOldOnError?t=o._rollbarOldOnError:r.onerror&&!r.onerror.belongsToShim&&(t=r.onerror,o._rollbarOldOnError=t);var a=function(){var n=Array.prototype.slice.call(arguments,0);e(r,o,t,n)};a.belongsToShim=n,r.onerror=a}}function e(r,o,n,e){r._rollbarWrappedError&&(e[4]||(e[4]=r._rollbarWrappedError),e[5]||(e[5]=r._rollbarWrappedError._rollbarContext),r._rollbarWrappedError=null),o.handleUncaughtException.apply(o,e),n&&n.apply(r,e)}function t(r,o,n){if(r){"function"==typeof r._rollbarURH&&r._rollbarURH.belongsToShim&&r.removeEventListener("unhandledrejection",r._rollbarURH);var e=function(r){var n=r.reason,e=r.promise,t=r.detail;!n&&t&&(n=t.reason,e=t.promise),o&&o.handleUnhandledRejection&&o.handleUnhandledRejection(n,e)};e.belongsToShim=n,r._rollbarURH=e,r.addEventListener("unhandledrejection",e)}}function a(r,o,n){if(r){var e,t,a="EventTarget,Window,Node,ApplicationCache,AudioTrackList,ChannelMergerNode,CryptoOperation,EventSource,FileReader,HTMLUnknownElement,IDBDatabase,IDBRequest,IDBTransaction,KeyOperation,MediaController,MessagePort,ModalWindow,Notification,SVGElementInstance,Screen,TextTrack,TextTrackCue,TextTrackList,WebSocket,WebSocketWorker,Worker,XMLHttpRequest,XMLHttpRequestEventTarget,XMLHttpRequestUpload".split(",");for(e=0;e<a.length;++e)t=a[e],r[t]&&r[t].prototype&&l(o,r[t].prototype,n)}}function l(r,o,n){if(o.hasOwnProperty&&o.hasOwnProperty("addEventListener")){for(var e=o.addEventListener;e._rollbarOldAdd&&e.belongsToShim;)e=e._rollbarOldAdd;var t=function(o,n,t){e.call(this,o,r.wrap(n),t)};t._rollbarOldAdd=e,t.belongsToShim=n,o.addEventListener=t;for(var a=o.removeEventListener;a._rollbarOldRemove&&a.belongsToShim;)a=a._rollbarOldRemove;var l=function(r,o,n){a.call(this,r,o&&o._wrapped||o,n)};l._rollbarOldRemove=a,l.belongsToShim=n,o.removeEventListener=l}}r.exports={captureUncaughtExceptions:n,captureUnhandledRejections:t,wrapGlobals:a}},function(r,o){"use strict";function n(r,o){this.impl=r(o,this),this.options=o,e(n.prototype)}function e(r){for(var o=function(r){return function(){var o=Array.prototype.slice.call(arguments,0);if(this.impl[r])return this.impl[r].apply(this.impl,o)}},n="log,debug,info,warn,warning,error,critical,global,configure,handleUncaughtException,handleUnhandledRejection,_createItem,wrap,loadFull,shimId".split(","),e=0;e<n.length;e++)r[n[e]]=o(n[e])}n.prototype._swapAndProcessMessages=function(r,o){this.impl=r(this.options);for(var n,e,t;n=o.shift();)e=n.method,t=n.args,this[e]&&"function"==typeof this[e]&&this[e].apply(this,t);return this},r.exports=n},function(r,o){"use strict";r.exports=function(r){return function(o){if(!o&&!window._rollbarInitialized){r=r||{};for(var n,e,t=r.globalAlias||"Rollbar",a=window.rollbar,l=function(r){return new a(r)},i=0;n=window._rollbarShims[i++];)e||(e=n.handler),n.handler._swapAndProcessMessages(l,n.messages);window[t]=e,window._rollbarInitialized=!0}}}}]);
// End Rollbar Snippet
	console.log('rollbar loaded', window.onerror);
}

function instabugInit(t){
	$.getScript('https://s3.amazonaws.com/instabug-pro/sdk_releases/instabugsdk-1.1.3-beta.min.js', function(){
		ibgSdk.init({
			token: 'fb8325ff72395bc04dbf8a9d2d191765'
		});
		console.log('loaded instabug');
	});
}

function sentryInit(t){
	//if (env() == 'dev') return;

	Raven.config('https://1fe5c4ff0d4843e68af6ec3e558217d5@sentry.io/193483').install();
	console.log('loaded sentry.io');	
	//Raven.captureMessage('app was restarted ' + new Date());
	t.autorun(()=>{
		if (Meteor.user())
			Raven.setUserContext({
				id: Meteor.userId(),
				username: Meteor.user().username,
			});
		else if	(Session.get('virgoUserId'))
			Raven.setUserContext({
				id: Session.get('virgoUserId').userId,
				username: 'anonymous',
			});
	});

/* 	$.getScript('https://cdn.ravenjs.com/3.17.0/raven.min.js', function(){
		Raven.config('https://1fe5c4ff0d4843e68af6ec3e558217d5@sentry.io/193483').install()
		console.log('loaded sentry.io');
		t.autorun(()=>{
			if (Meteor.user())
				Raven.setUserContext({
					id: Meteor.userId(),
					username: Meteor.user().username,
				});
			else if	(Session.get('virgoUserId'))
				Raven.setUserContext({
					id: Session.get('virgoUserId').userId,
					username: 'anonymous',
				});
		});
	}); */
}

function oribiInit(t){
  (function(b, o, n, g, s, r, c) { if (b[s]) return; b[s] = {}; b[s].scriptToken = "Xy01NDg2OTYxMzY"; r = o.createElement(n); c = o.getElementsByTagName(n)[0]; r.async = 1; r.src = g; r.id = s + n; c.parentNode.insertBefore(r, c); })(window, document, "script", "//cdn.oribi.io/Xy01NDg2OTYxMzY/oribi.js", "ORIBI");
}

function analyticsAll(t){
	if (navigator.userAgent.match(/seo4ajax|prerender/)) return;
	var env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app/);
	if (env) env = env[0];
	
	t.autorun(function(){
		var data, list = {common: true} ;
/* 		if (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group'))
			list = {personal: true}; */
		data = Collections.Settings.find(list).fetch();
		if (!data.length)
			return console.warn('layout.onRendered integration exit', list, data);
		if (Session.get('debug')) console.log('layout.onRendered integration', list, data);
		if (_.findWhere(data,{type: 'fullstory'}) && _.findWhere(data,{type: 'fullstory'}).enable) {
			if (FlowRouter.getParam('id') != 'eY7e6ArDJkrKobyga')
				initFullH();
			if (Session.get('debug')) console.log('layout onRendered enabled fullstory', data, _.findWhere(data,{type: 'fullstory'})); 
		}
		if (_.findWhere(data,{type: 'localytics'}) && _.findWhere(data,{type: 'localytics'}).enable) {
			localytics();
			if (Session.get('debug')) console.log('layout onRendered enabled localytics', data, _.findWhere(data,{type: 'localytics'})); 
		}		
		if (_.findWhere(data,{type: 'leanplum'}) && _.findWhere(data,{type: 'leanplum'}).enable) {
			leanplumInit();
			if (Session.get('debug')) console.log('layout onRendered enabled leanplum', data, _.findWhere(data,{type: 'leanplum'})); 
		}
		if (_.findWhere(data,{type: 'kuoll'}) && _.findWhere(data,{type: 'kuoll'}).enable) {
			//kuollInit();
			if (Session.get('debug')) console.log('layout onRendered enabled kuoll', data, _.findWhere(data,{type: 'kuoll'})); 
		}
		if (_.findWhere(data,{type: 'segmentio'}) && _.findWhere(data,{type: 'segmentio'}).enable) {
			segmentio();
			if (Session.get('debug')) console.log('layout onRendered enabled segmentio', data, _.findWhere(data,{type: 'segmentio'})); 
		}		
		if (_.findWhere(data,{type: 'rollbar'}) && _.findWhere(data,{type: 'rollbar'}).enable) {
			rollbarInit(t);
			if (Session.get('debug')) console.log('layout onRendered enabled rollbar', data, _.findWhere(data,{type: 'rollbar'})); 
		}		
		if (_.findWhere(data,{type: 'olark'}) && _.findWhere(data,{type: 'olark'}).enable) {
			olarkInit(t);
			if (Session.get('debug')) console.log('layout onRendered enabled olark', data, _.findWhere(data,{type: 'olark'})); 
		}
		if (_.findWhere(data,{type: 'canny'}) && _.findWhere(data,{type: 'canny'}).enable) {
			cannyInit(t);
			if (Session.get('debug')) console.log('layout onRendered enabled canny', data, _.findWhere(data,{type: 'canny'})); 
		}
		//instabugInit(t);
		if (document.URL.match(/www/))
			sentryInit(t);
		heapInit(t);
		oribiInit(t);
		
	});

	// user flow
	t.autorun(function(){
		if (!env) return;
		if (!FlowRouter.getRouteName())
			return;
		if (window.ll) ll('tagScreen', document.title);
		if (window.ga) ga('require', 'GTM-WSZP4Z2');
		//analytics.page(document.title);
	});	
	
	
	// tag event
/* 	t.autorun(function(){
		if (!env) return;
		var report = _.extend(FlowRouter.current().params, FlowRouter.current().queryParams);
		report.layout = FlowRouter.getRouteName();
		if (Session.get('debug')) console.log('ll tagEvent report', FlowRouter.getRouteName(), report);
		if (window.ll) ll('tagEvent', FlowRouter.getRouteName(), report);		
		analytics.track("user event", {
			eventName: FlowRouter.current().path,
		});		
	}); */
	// identify user
	t.autorun(function(){
		var email;
		if (Meteor.user()){
			if (Meteor.user().emails)
				email = Meteor.user().emails[0];
			if (email && email.length) email = Meteor.user().emails[0].email;
			if (window.analytics && !analytics.user().id() ) analytics.identify(Meteor.userId(),{
					"username": Meteor.user().username,
					"email": email,
					"env": env
				},
			);
		
			if (window.FS)
				FS.identify(Meteor.userId() , {
					displayName: Meteor.user().username,
					profile: 'https://www.360zipper.com/user/' + Meteor.userId()
				});		
				
			if (window.ll) {
				ll('setCustomerId', Meteor.userId());
				ll('setProfileAttribute', 'profile', 'https://www.360zipper.com/user/' + Meteor.userId(), 'org');
			}
			
			if (window.ga) ga('set', 'userId', Meteor.userId());
		}
		//if (Session.get('virgoUserId')) analytics.identify(Session.get('virgoUserId').userId ,{username: Session.get('virgoUserId').username});
	});
	
	// remember referrer
	t.autorun(function(){
		if (!Session.get('referrer') && document.referrer && !document.referrer.match(/360zipper.com/))
			Session.setPersistent('referrer', {ref: document.referrer, link: FlowRouter.getQueryParam('link'), date: new Date()});
		if (Session.get('referrer') && Meteor.user() && !Meteor.user().profile.referred){
			Meteor.call('user.update.ref', {debug: Session.get('debug'), referred: Session.get('referrer')});
			//Meteor.users.update(Meteor.userId(),{$set: {'profile.referred': Session.get('referrer')}});
		}
	});	
}

Template.layout.onCreated( () => {
//	import '/imports/external/grayscale.js';
	//console.log('if iframe', window.frameElement);
	
	$('#loadingspinner').fadeOut('slow');
	let t = Template.instance();
	t.olark = new ReactiveVar ();
	t.openComments = new ReactiveVar ();
	t.message = new ReactiveVar ();
	t.answered = new ReactiveVar ();
	t.fromsearch = new ReactiveVar ();
	t.operator = new ReactiveVar ();
	
	Meteor.subscribe('settings', {caller:'layout', navigator: navigator.userAgent, referrer: document.referrer});
	t.autorun(()=>{
		if (Meteor.userId()) Meteor.subscribe('userdata', {userId: Meteor.userId()});
	});
//	SEO = new FlowRouterSEO();
	
	t.autorun(function(){
		if (FlowRouter.current())
			window.prerenderReady = false;
	});
	t.autorun(function(){
		if (Meteor.userId()) Meteor.call('user.visited');
	});
	t.autorun(()=>{
		if (FlowRouter.getRouteName())
			$('html, body').animate({scrollTop: $('body').offset().top - 200 });
	})
	t.autorun(()=>{
		if (Session.get('virgoUserId'))
			Meteor.subscribe('conversation', {user: Session.get('virgoUserId')});
	});
	
});
Template.layout.onRendered( () => {


});
Template.layout.helpers({

});
Template.layout.events({
	'click .pointer' (e,t){
		// saving event
		var env = __meteor_runtime_config__.ROOT_URL.match(/www|stg|app/);
		if (!env) return;
		var report = _.extend(FlowRouter.current().params, FlowRouter.current().queryParams);
		report.layout = FlowRouter.getRouteName();
		if (Session.get('debug')) console.log('ll tagEvent report', FlowRouter.getRouteName(), report);
		if (window.ll) ll('tagEvent', FlowRouter.getRouteName(), report);		
/* 		analytics.track("user event", {
			eventName: FlowRouter.current().path,
		});		
		analytics.page({
			title: 'Signup Modal',
			url: 'https://segment.com/#signup',
			path: '/#signup',
			referrer: 'https://segment.com/'
		});	 */	
	},


});

Template.layout404.onCreated( () => {

});
Template.layout404.onRendered( () => {
	$('#loadingspinner').fadeOut('slow');
	$('#injectloadingspinner').fadeOut();
	$('body').css('overflow', 'auto');
});