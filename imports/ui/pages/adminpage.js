import { AutoForm } from 'meteor/aldeed:autoform';
import { HTTP } from 'meteor/http';
//Images.attachSchema(SchemasImages);
import './adminpage.html';

import { Schemas } from '/imports/api/links/collections.js';
import { Collections } from '/imports/api/links/collections.js';

import { Push } from '/imports/api/links/collections.js';
import { Contact } from '/imports/api/links/collections.js';
import { Texts } from '/imports/api/links/collections.js';
import { EmailsTmpl } from '/imports/api/links/collections.js';
import { Faq } from '/imports/api/links/collections.js';
import { Pricing } from '/imports/api/links/collections.js';
import { ProUsers } from '/imports/api/links/collections.js';


import { Sounds } from '/imports/api/links/collections.js';
import { LocalFiles } from '/imports/api/links/collections.js';
//import { GoogleGiles } from '/imports/api/links/collections.js';
import { hooksAddFile } from '/imports/api/hooks.js';

window.LocalFiles = LocalFiles;
//LocalFiles.attachSchema(Schemas.LocalFiles);
AutoForm.addHooks('insertFilesForm', hooksAddFile);

window.Texts = Texts;
window.EmailsTmpl = EmailsTmpl;
window.Faq = Faq;
window.Pricing = Pricing;

function checkMore(t){
	Meteor.setTimeout(function(){
		if ($('#infiniteCheck').offset() && $('#infiniteCheck').offset().top - $(window).scrollTop() - $(window).height() < -20){
			t.limit.set(t.limit.get() + t.next.get());
			if (Session.get('debug')) console.log('getting next limit 0:', t.ready.get(), t.limit.get(), t.next.get(), t.loaded.get(), $('#infiniteCheck').offset().top - $(window).scrollTop() - $(window).height());
		}	
	},500);
}

function convertToCSV(objArray) {
	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	var str = '';

	for (var i = 0; i < array.length; i++) {
		var line = '';
		for (var index in array[i]) {
			if (line != '') line += ','
			line += array[i][index];
		}
		str += line + '\r\n';
	}

	return str;
}

Template.adminpage.onCreated( () => {
	//AutoForm.debug(); 
	Meteor.subscribe('contact');
	Meteor.subscribe('texts');
	Meteor.subscribe('faq');
	Meteor.subscribe('pricing');
	Meteor.subscribe('verrors');
	let t = Template.instance();
	t.editpage = new ReactiveVar();
	t.showcontacts = new ReactiveVar();
	t.newRecord = new ReactiveVar();
	t.editRecord = new ReactiveVar();
	t.selectText = new ReactiveVar('landing');
	if (FlowRouter.getQueryParam('get'))
		t.selector = new ReactiveVar(FlowRouter.getQueryParam('get'));
	else
		t.selector = new ReactiveVar();
});
Template.adminpage.onRendered( () => {
	let t = Template.instance();
	t.autorun(function(){
		console.log('adminPage editor', t.editRecord.get());
/* 		$('textarea').froalaEditor({
			fontFamily: {
				"Roboto,sans-serif": 'Roboto',
				"Oswald,sans-serif": 'Oswald',
				"Montserrat,sans-serif": 'Montserrat',
				"'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
			},
			fontFamilySelection: true
		});	 */			
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				if (!$(this).hasClass('olark-form-message-input')) 
					$('textarea').each(function(){
						CKEDITOR.replace($(this).attr('name'))
					});
		},500);
		// Meteor.setTimeout(function(){
			// $('*').filter(function() {
				// if ($(this).text() == 'Unlicensed Froala Editor') $(this).remove()
			// });
		// }, 200);		
	});

});
Template.adminpage.helpers({
	selector(){
		let t = Template.instance();
		//console.log('selector', t.selector.get());
		return t.selector.get();
	},
	filename(){
		return this.model.split('/').pop();
	},
	ImagesColl() {
		return Images;
	},
/* 	showContacts(){
		return Template.instance().showcontacts.get();
	}, */
	newRecord(){
		return Template.instance().newRecord.get();
	},
	editRecord(){
		return Template.instance().editRecord.get();
	},
	faqs(){
		return Faq.find();
	},
	credits(){
		return Faq.find();
	},
  contacts() {
		var contacts = Contact.find();
		//console.log('uploaded tour', tour, Template.instance().tour.get());
		if (contacts.count() == 0)
			return false;
		return contacts;
	},
	ifSeen(){
		var data =  _.findWhere(this.seen, Meteor.userId());
		return data;
	},
	texts(){
		return Texts.find();
	},
	dataTexts(){
		let t = Template.instance();
		var data = Texts.findOne({title:t.selectText.get()});
		console.log('dataText', data, t.selectText.get());
		return data;
	},	
	news(){
		return Texts.find({title:'productnews'},{sort: {createdAt: -1}});
	},
	formId(){
		return this._id + 'updateForm';
	},
	dataPricing(){
		return Pricing.findOne();
	},	
	dataEmails(){
		var data = Emails.find();
		if (data.count())
			return data;
	},

  error() {
    return Template.instance().error.get();
  }
});
Template.adminpage.events({
	'click .pointer': function (e, t) {
		console.log('clicked cursor', e.target.id, this);
		
		if (e.target.id && e.target.id != t.selector.get()) {
			t.selector.set(e.target.id);	
			FlowRouter.setQueryParams({get: e.target.id});
		} else {
			t.selector.set();
			FlowRouter.setQueryParams({get: null});
			FlowRouter.setQueryParams({userId: null});
		}
		
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				if (!$(this).hasClass('olark-form-message-input')) 
					$('textarea').each(function(){
						CKEDITOR.replace($(this).attr('name'))
					});
		},500);
	},
  'change [name="uploadJSON"]' ( event, template ) {

  },
  'click .seen' ( event, template ) {
		console.log('seen', event, 	event.target, event.target.id);
		Contact.update(event.target.id, {$push: {seen: Meteor.userId()}});
		//Meteor.call('remove.contact', event.target._id);
  },
  'click .remove' ( event, template ) {
		console.log('remove', event, 	event.target, event.target.id);
		Contact.remove(event.target.id);
		//Meteor.call('remove.contact', event.target._id);
  },
  'click .newRecord' ( e, t ) {
		console.log('newRecord', this._id, e.target, this);
		t.editRecord.set('new');
  },  
	'click .editRecord' ( e, t ) {
		console.log('editRecord', this._id, e.target, this);
		t.editRecord.set(this._id);
  },
	'click .submitted'(e,t){
		t.editRecord.set();
		Bert.alert('submitted','success');
	},
	'change .selectText'(e,t){
		t.selectText.set($( "#selectText" ).val());
		console.log('selectText', t.selectText.get(), e.target, t.data, this);
	}
});

Template.bannedTours.onCreated( () => {
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.vrloaded = new ReactiveVar();
	t.showtime = new ReactiveVar();
  t.subscribe('tours');
	t.subscribe('images');
	t.subscribe('showtime');
});
Template.bannedTours.helpers({
	tours(){
		var data = Tours.find({hidden: true});
		console.log('banned worlds', data.fetch());
		return data;
	},
	dataImg(){
		let t = Template.instance();
		t.subscribe('images', this._id);
		//console.log('tourImg', this);
		var tour = Tours.findOne(this._id);
		var cloudIcon = 'c_fill,h_128,w_256/q_auto,f_auto,dpr_auto,e_improve/';	
		this.cloudenv.cloudThumb = Meteor.settings.public.CLOUDINARY_THUMB + '/';
		this.cloudenv.cloudPreview = Meteor.settings.public.CLOUDINARY_PREVIEW + '/';
		var path = this.cloudenv.cloudPath + this.cloudenv.cloudPreview + this.cloudenv.cloudFolder;
		if (!tour || !tour.scene_id || !tour.scene_id[0])
			return '/img/loading.gif';
		var panorama = Images.findOne({scene_id:tour.scene_id[0]});	
		if (this.icon && this.icon.cloud) 
			return path + this.icon.cloud;
		else if (panorama) 
			return path + panorama.cloud;
		else
			return '/img/loading.gif';
	},
	grayscale(){
		console.log('grayscale', this.hidden, this);
		if (this.hidden)
			return 'grayscale';
	},
});
Template.bannedTours.events({
  'click .askUnhide' ( t, t ) {
		console.log('remove', event.target.id, this);
		Tours.update(this._id,{$set:{hidden:false}});
		//Meteor.call('remove.contact', event.target._id);
  },
});

Template.conditions.onCreated( () => {
	AutoForm.debug(); 
	//Meteor.subscribe('contact');
	//Meteor.subscribe('texts');
	//Meteor.subscribe('faq');
	Template.instance().editpage = new ReactiveVar();
	Template.instance().showcontacts = new ReactiveVar();
	Template.instance().newFaq = new ReactiveVar();
	Template.instance().editFaq = new ReactiveVar();
});
Template.conditions.helpers({
	conditions(){
		var data;
		data = Texts.findOne({title: 'landing'},{fields:{'conditions':1}});
		console.log('conditions', data);
		return data;
	},
});
Template.conditions.events({
  'change [name="uploadJSON"]' ( event, template ) {

  },
  'click .remove' ( event, template ) {
		console.log('remove', event, 	event.target, event.target.id);
		Contact.remove(event.target.id);
		//Meteor.call('remove.contact', event.target._id);
  },
  'click .pointer' ( event, template ) {
		template.editpage.set(false);
		if (event.target.id == 'new')
			event.target.id = false;
//		console.log('pointer', event, 	event.target, event.target.id, template.editpage.get());
		Meteor.setTimeout(function(){template.editpage.set(event.target.id)},10);
		//Meteor.call('remove.contact', event.target._id);
  },
  'click #showContacts' ( event, template ) {
//		console.log('pointer', event, 	event.target, event.target.id, template.editpage.get());
		template.showcontacts.set(!template.showcontacts.get());
		//Meteor.call('remove.contact', event.target._id);
  },
  'click .editFaq' ( e, t ) {
		console.log('editFaq', this._id, e.target, this);
		t.editFaq.set(this._id);
  },
});

Template.conditions.onCreated( () => {
	Meteor.subscribe('userdata');
});

Template.sharedtours.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	const t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('tours',{
			social: true,
			limit: t.limit.get(),
			sort: t.sort.get(),
			debug: Session.get('debug')
		});
		t.ready.set(sub.ready());
	})	
	t.autorun(function(){
		if (t.ready.get())
			t.loaded.set(Tours.find({social:{$exists: true}}).count());
		if (Session.get('debug')) console.log('subusers.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.sharedtours.onRendered(() => {
	const t = Template.instance();
	// t.autorun(()=>{
		// if (!t.ready.get())
			// return;
		// Meteor.setTimeout(()=>{
			// $('.toggleSwitch').bootstrapToggle('destroy');
			// $('.toggleSwitch').bootstrapToggle();
		// },500);
	// });
	Meteor.setTimeout(()=>{
		// $('.toggleSwitch').bootstrapToggle('destroy');
		// $('.toggleSwitch').bootstrapToggle();
	},1500);
});
Template.sharedtours.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	tours(){
		var data = Tours.find({social:{$exists: true}},{sort:{'social.date': -1}});
		console.log('sharedtours tours', data.count(), data.fetch());
		return data;
	},
	sharedTimes(){
		return this.social.length;
	},	
	createdDate(){
		return this.createdAt.toLocaleDateString();
	},
	date(){
		if (this.date)
			return this.date.toLocaleDateString();
	},		
	url(){
		var url;
		if (this.type == 'fb')
			url = 'https://www.facebook.com/photo.php?fbid=' + this.id;
		else if (this.type == 'twitter')
			url = 'https://twitter.com/' + this.socialUser + '/status/' + this.id;		
		else if (this.type == 'telegram')
			url = 'https://t.me/' + this.id;		
		else if (this.type == 'medium')
			url = 'https://medium.com/' + this.id;
		return url
	},
	debug(){
		console.log('debug subusers', this);
	}
});
Template.sharedtours.events({
	'click .toggleSwitch'(e,t){
		var pushing;
		console.log('clicked togglePush', e.target.checked, this.pushing, '\n', e, this);
		if (this.pushing)
			pushing = null;
		else
			pushing = 'checked';
		Meteor.call('push.update', {_id: this._id, pushing: pushing});
	},
	'click .fbmessenger'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked fbmessenger', this);
		Modal.show('sendMessageModal', this);
	}
});

Template.subusers.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	const t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.filter = new ReactiveVar('all');
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('push',{
			all: true,
			limit: t.limit.get(),
			sort: t.sort.get(),
			filter: t.filter.get(),
			debug: Session.get('debug')
		});
		t.ready.set(sub.ready());
	})	
	t.autorun(function(){
		if (t.ready.get())
			t.loaded.set(Push.find().count());
		if (Session.get('debug')) console.log('subusers.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.subusers.onRendered(() => {
	const t = Template.instance();
	// t.autorun(()=>{
		// if (!t.ready.get())
			// return;
		// Meteor.setTimeout(()=>{
			// $('.toggleSwitch').bootstrapToggle('destroy');
			// $('.toggleSwitch').bootstrapToggle();
		// },500);
	// });
	Meteor.setTimeout(()=>{
		// $('.toggleSwitch').bootstrapToggle('destroy');
		// $('.toggleSwitch').bootstrapToggle();
	},1500);
});
Template.subusers.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	filter(){
		let t = Template.instance();
		return t.filter.get();		
	},
	user(){
		let t = Template.instance();
		var filter = {};
		if (t.filter.get() == 'all')
			filter = {};
		else if (t.filter.get() == 'checked')
			filter = {pushing: 'checked'};
		else if (t.filter.get() == 'nonchecked')
			filter = {
				$or: [
					{pushing: {$exists: false}},
					{pushing: null}
				]
			};
		var data = Push.find(filter,{sort: t.sort.get()});
		console.log('sub users', t.filter.get(), t.sort.get(), data.count(), data.fetch());
		return data;
	},
	userCount(){
		let t = Template.instance();
		var count = Meteor.call('count.push', (e,r)=>{
			t.count.set(r);
		});
		console.log('userCount', t.count.get());
		return t.count.get();
	},
	tokenLength(){
		if (this.token)
			return this.token.length;
	},
	date(){
		// $('.toggleSwitch').bootstrapToggle('destroy');
		// $('.toggleSwitch').bootstrapToggle();
		if (this.createdAt)
			return this.createdAt.toLocaleDateString();
	},	
	last(){
		if (this.sentAt)
			return this.sentAt.toLocaleDateString();
	},
	enabled(){
		var enabled;
		if (this.pushing) 
			enabled = 'checked';
		else
			enabled = null;
		console.log('push enabled', enabled, this.pushing, this)
		return enabled;
	},
	serviceAvatar(){
		console.log('serviceAvatar', this);
		var user = Meteor.users.findOne(this.userId);
		if (user)
			return user.profile.avatar;
	},
	debug(){
		console.log('debug subusers', this);
	}
});
Template.subusers.events({
	'click .toggleSwitch'(e,t){
		var pushing;
		console.log('clicked togglePush', e.target.checked, this.pushing, '\n', e, this);
		if (this.pushing)
			pushing = null;
		else
			pushing = 'checked';
		Meteor.call('push.pushing', {_id: this._id, pushing: pushing});
	},
	'click .pushmessage'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked pushmessage', this);
		Modal.show('sendPushModal', this);
	},
	'click .remove'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked remove', this);
		Push.remove(this._id);
	},
	'click .filter'(e,t){
		if (!t.filter.get() || t.filter.get() == 'all')
			t.filter.set('checked');
		else if (t.filter.get() == 'checked')
			t.filter.set('nonchecked');		
		else if (t.filter.get() == 'nonchecked')
			t.filter.set('all');
		else
			t.filter.set('checked');
		console.log('click filter', t.filter.get(), e.target.id, e);
	},	
	'click .sort'(e,t){	
		if (e.target.id == 'signed')
			t.sort.set({createdAt: -1});		
		if (e.target.id == 'sent')
			t.sort.set({sentAt: -1});
	}
});

Template.adopters.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	let t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	t.showpro = new ReactiveVar(0);
	t.showtesters = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('adopters',{
			all: true,
			limit: t.limit.get(),
			sort: t.sort.get(),
			debug: Session.get('debug')
		});
	})	
	t.autorun(function(){
		if (sub)
			t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Collections.Adopters.find().count());
		if (Session.get('debug')) console.log('adopters.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.adopters.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	userCount(){
		let t = Template.instance();
		var count = Meteor.call('count.adopters', (e,r)=>{
			if (r) console.warn('ERR adopters call ', e,r);
			t.count.set(r);
		});
		//console.log('userCount', t.count.get());
		return t.count.get();
	},
	user(){
		var data = Collections.Adopters.find({},{sort:{createdAt: -1}});
		console.log('adopters', data.count(), data.fetch());
		return data;
	},
	date(){
		if (this.createdAt)
			return this.createdAt.toLocaleDateString();
	},	
	lastVisit(){
		if (this.visitedAt)
			return this.visitedAt.toLocaleDateString();
	},
});
Template.adopters.events({
  'click .submit' ( e, t ) {	
		var username = $('#username').val();
		console.log('submit username', $('#username').val());
		Meteor.call('user.addtester', {username: username, manage: 'add'});
  },
  'click .toggleTester' ( e, t ) {
		var tester, manage;
		console.log('toggleTester', e, this.roles, this );
		if (this.roles)
			tester = _.contains(_.flatten(_.values(this.roles)),'tester');
		if (tester) 
			manage = 'remove';
		else
			manage = 'add';
		Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },  
	'click .togglePro' ( e, t ) {
		var user, manage;
		var pro = ProUsers.findOne({userId: this._id});
		
		
		if (!pro)
			ProUsers.insert({userId: this._id, type: 'pro', max: Meteor.settings.public.ImageMaxPro, createdAt: new Date, expire: new Date(Date.now()+365*24*60*60*1000)});
		else
			ProUsers.remove(pro._id);
		pro = ProUsers.findOne({userId: this._id});
		console.log('togglePro', e, this.roles, this, 'pro:',  pro, Meteor.settings.public.ImageMaxPro);
		//Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },
	'click .fbmessenger'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked fbmessenger', this);
		Modal.show('sendMessageModal', this);
	},
	'click #showTesters'(e,t){
		var val = $('#showTesters').prop('checked');
		console.log('clicked showTesters', val, e.target, this);
		t.showtesters.set(val);
	},	
	'click #showPro'(e,t){
		var val = $('#showPro').prop('checked');
		console.log('clicked showPro', val, e.target, this);
		t.showpro.set(val);
	}
});

Template.conversations.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	let t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	t.showpro = new ReactiveVar(0);
	t.showtesters = new ReactiveVar(0);
	t.conversations = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('conversation',{userId: FlowRouter.getQueryParam('userId')});
	})	
	t.autorun(function(){
		if (sub)
			t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Collections.Conversation.find().count());
		if (Session.get('debug')) console.log('adopters.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
	Meteor.call('social.chatbot.conversations',(e,r)=>{
		console.log('uniq userId conversations', r); 
		t.conversations.set(r);
	});
	t.autorun(function(){	
		sub = t.subscribe('conversation',{userId: FlowRouter.getQueryParam('userId')});
	})
});
Template.conversations.onDestroyed( () => {
	FlowRouter.setQueryParams({userId: null});
});
Template.conversations.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	currentUser(){
		return FlowRouter.getQueryParam('userId');		
	},
	users(){
		let t = Template.instance();
		return t.conversations.get();
	},
	conversations(){
		return Collections.Conversation.find({userId: FlowRouter.getQueryParam('userId')}, {sort: {createdAt: -1}});
	}
});
Template.conversations.events({
  'click .remove' ( e, t ) {	
		Collections.Conversation.remove(this._id);
  },
});

Template.conversation.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	let t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	t.showpro = new ReactiveVar(0);
	t.showtesters = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('conversation',{userId: FlowRouter.getQueryParam('userId')});
	})	
	t.autorun(function(){
		if (sub)
			t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Collections.Conversation.find().count());
		if (Session.get('debug')) console.log('adopters.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.conversation.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	userCount(){
		let t = Template.instance();
		var count = Meteor.call('count.adopters', (e,r)=>{
			if (r) console.warn('ERR adopters call ', e,r);
			t.count.set(r);
		});
		//console.log('userCount', t.count.get());
		return t.count.get();
	},
	user(){
		var data = Collections.Adopters.find({},{sort:{createdAt: -1}});
		console.log('adopters', data.count(), data.fetch());
		return data;
	},
	date(){
		if (this.createdAt)
			return this.createdAt.toLocaleDateString();
	},	
	lastVisit(){
		if (this.visitedAt)
			return this.visitedAt.toLocaleDateString();
	},
});
Template.conversation.events({
  'click .submit' ( e, t ) {	
		var username = $('#username').val();
		console.log('submit username', $('#username').val());
		Meteor.call('user.addtester', {username: username, manage: 'add'});
  },
  'click .toggleTester' ( e, t ) {
		var tester, manage;
		console.log('toggleTester', e, this.roles, this );
		if (this.roles)
			tester = _.contains(_.flatten(_.values(this.roles)),'tester');
		if (tester) 
			manage = 'remove';
		else
			manage = 'add';
		Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },  
	'click .togglePro' ( e, t ) {
		var user, manage;
		var pro = ProUsers.findOne({userId: this._id});
		
		
		if (!pro)
			ProUsers.insert({userId: this._id, type: 'pro', max: Meteor.settings.public.ImageMaxPro, createdAt: new Date, expire: new Date(Date.now()+365*24*60*60*1000)});
		else
			ProUsers.remove(pro._id);
		pro = ProUsers.findOne({userId: this._id});
		console.log('togglePro', e, this.roles, this, 'pro:',  pro, Meteor.settings.public.ImageMaxPro);
		//Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },
	'click .fbmessenger'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked fbmessenger', this);
		Modal.show('sendMessageModal', this);
	},
	'click #showTesters'(e,t){
		var val = $('#showTesters').prop('checked');
		console.log('clicked showTesters', val, e.target, this);
		t.showtesters.set(val);
	},	
	'click #showPro'(e,t){
		var val = $('#showPro').prop('checked');
		console.log('clicked showPro', val, e.target, this);
		t.showpro.set(val);
	}
});

Template.allusers.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	let t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	t.showpro = new ReactiveVar(0);
	t.showtesters = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('userdata',{
			all: true,
			limit: t.limit.get(),
			sort: t.sort.get(),
			debug: Session.get('debug')
		});
	})	
	t.autorun(function(){
		if (sub)
			t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Meteor.users.find().count());
		if (Session.get('debug')) console.log('allusers.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});

  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.allusers.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	userCount(){
		let t = Template.instance();
		var count = Meteor.call('count.allusers', (e,r)=>{
			if (r) console.warn('ERR userCount call ', e,r);
			t.count.set(r);
		});
		//console.log('userCount', t.count.get());
		return t.count.get();
	},
	user(){
		var data = Meteor.users.find({},{fields:{'services.password': 0, 'services.resume': 0}, sort:{visitedAt: -1}});
		//console.log('all users', data.count(), data.fetch());
		return data;
	},
	roles(){
		if (this.roles)
			return _.values(this.roles);
	},
	subscribed(){
		let t = Template.instance();
		var sub = t.subscribe('prousers',{userId: this._id});
		//console.log('subscribed prousers sub', this, sub, sub.ready());
		return sub.ready();
	},
	prouser(){
		var data = ProUsers.findOne({userId: this._id});
		//console.log('subscribed prousers sub', this, data);
		return data;
	},
	date(){
		if (this.createdAt)
			return this.createdAt.toLocaleDateString();
	},	
	lastVisit(){
		if (this.visitedAt)
			return this.visitedAt.toLocaleDateString();
	},
	userservices(){
		//console.log('allusers user', this);
		if (!this.services)
			return;
		var services = [];
		_.each(_.pairs(this.services), function(pair){
			if (pair[0] == 'twitter')
				pair[1].link = 'https://twitter.com/' + pair[1].screenName;
			else if (pair[0] == 'google')
				pair[1].link = 'https://plus.google.com/u/' + pair[1].id;
			services.push({service: pair[0], value: pair[1]});
		});		
		//console.log('services', services);
		return services;
	},
	serviceAvatar(){
		//console.log('serviceAvatar', this);
		return this.value.avatar || this.value.picture || this.value.profile_image_url_https;
	}
/* 	userdata(){
		var data = Meteor.users.findOne(this.userId);
		if (!data)
			PostSubs.subscribe('userdata',{userId: this.userId});
		console.log('pro users', this.userId, data);
		return data;
	} */
});
Template.allusers.events({
  'click .submit' ( e, t ) {	
		var username = $('#username').val();
		console.log('submit username', $('#username').val());
		Meteor.call('user.addtester', {username: username, manage: 'add'});
  },
  'click .toggleTester' ( e, t ) {
		var tester, manage;
		console.log('toggleTester', e, this.roles, this );
		if (this.roles)
			tester = _.contains(_.flatten(_.values(this.roles)),'tester');
		if (tester) 
			manage = 'remove';
		else
			manage = 'add';
		Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },  
	'click .togglePro' ( e, t ) {
		var user, manage;
		var pro = ProUsers.findOne({userId: this._id});
		
		
		if (!pro)
			ProUsers.insert({userId: this._id, type: 'pro', max: Meteor.settings.public.ImageMaxPro, createdAt: new Date, expire: new Date(Date.now()+365*24*60*60*1000)});
		else
			ProUsers.remove(pro._id);
		pro = ProUsers.findOne({userId: this._id});
		console.log('togglePro', e, this.roles, this, 'pro:',  pro, Meteor.settings.public.ImageMaxPro);
		//Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },
	'click .fbmessenger'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked fbmessenger', this);
		Modal.show('sendMessageModal', this);
	},
	'click #showTesters'(e,t){
		var val = $('#showTesters').prop('checked');
		console.log('clicked showTesters', val, e.target, this);
		t.showtesters.set(val);
	},	
	'click #showPro'(e,t){
		var val = $('#showPro').prop('checked');
		console.log('clicked showPro', val, e.target, this);
		t.showpro.set(val);
	}
});

Template.mailchimp.onCreated( () => {
	AutoForm.debug(); 
	var sub;
	//PostSubs.subscribe('userdata', {limit: 10});
	let t = Template.instance();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar(true);
	t.next = new ReactiveVar(5);
	t.limit = new ReactiveVar(10);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
	t.count = new ReactiveVar(0);
	t.showpro = new ReactiveVar(0);
	t.showtesters = new ReactiveVar(0);
	t.showwith = new ReactiveVar(0);
	t.file = new ReactiveVar(0);
	
	t.autorun(function(){	
		sub = t.subscribe('userdata',{
			all: true,
			limit: t.limit.get(),
			sort: t.sort.get(),
			debug: Session.get('debug')
		});
	})	
	t.autorun(function(){
		if (sub)
			t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Meteor.users.find().count());
		if (Session.get('debug')) console.log('allusers.onCreated sub ready', t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	});
	var count = Meteor.call('count.allusers', (e,r)=>{
		if (r) console.warn('ERR userCount call ', e,r);
		t.count.set(r);
	});
  $(window).scroll(function(){	
		checkMore(t);
	});
});
Template.mailchimp.helpers({
	isMore(){
		let t = Template.instance();		
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		if (Session.get('debug')) console.log('isMore', t.limit.get(), t.next.get(), t.loaded.get());
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		return t.ready.get();
	},
	userCount(){
		let t = Template.instance();
		//console.log('userCount', t.count.get());
		return t.count.get();
	},
	user(){
		let t = Template.instance();
		var list = {};
		if (t.showwith.get() =='withTours')
			list = {'profile.count': {$elemMatch: {type: 'published', number: {$gte: 1}}}};
		else if (t.showwith.get() =='withNoPublishes')
			list = { $and: [{'profile.count': {$elemMatch: {type: 'published', number: 0}}}, {'profile.count': {$elemMatch: {type: 'tours', number: {$gte: 1}}}}]};
		else if (t.showwith.get() =='withNoPanos')
			list = {'profile.count': {$elemMatch: {type: 'panos', number: 0}}};

		var data = Meteor.users.find(list,{fields:{username:1, emails: 1, 'profile.firstName': 1, 'profile.lastName': 1, 'profile.count': 1}, sort:{visitedAt: -1}});
		//console.log('all users', data.count(), data.fetch());
		t.count.set(data.count());
		var file = Meteor.users.find(list,{fields:{username: 1, 'emails.address': 1, 'profile.firstName': 1, 'profile.lastName': 1}, sort:{visitedAt: -1}});
		if (file.count()) {
			file = _.map(file.fetch(), function(rec){
				console.log(rec); 
				var out = {}; 
				out.username = rec.username; 
				out.email = rec.emails[0].address; 
				out.firstName = rec.profile.firstName; 
				out.lastName = rec.profile.lastName; 		
				return out
			});
			t.file.set(file);
		}
		return data;
	},
	roles(){
		if (this.roles)
			return _.values(this.roles);
	},
	subscribed(){
		let t = Template.instance();
		var sub = t.subscribe('prousers',{userId: this._id});
		//console.log('subscribed prousers sub', this, sub, sub.ready());
		return sub.ready();
	},
	prouser(){
		var data = ProUsers.findOne({userId: this._id});
		//console.log('subscribed prousers sub', this, data);
		return data;
	},
	date(){
		if (this.createdAt)
			return this.createdAt.toLocaleDateString();
	},	
	lastVisit(){
		if (this.visitedAt)
			return this.visitedAt.toLocaleDateString();
	},
	userservices(){
		//console.log('allusers user', this);
		if (!this.services)
			return;
		var services = [];
		_.each(_.pairs(this.services), function(pair){
			if (pair[0] == 'twitter')
				pair[1].link = 'https://twitter.com/' + pair[1].screenName;
			else if (pair[0] == 'google')
				pair[1].link = 'https://plus.google.com/u/' + pair[1].id;
			services.push({service: pair[0], value: pair[1]});
		});		
		//console.log('services', services);
		return services;
	},
	serviceAvatar(){
		//console.log('serviceAvatar', this);
		return this.value.avatar || this.value.picture || this.value.profile_image_url_https;
	}
/* 	userdata(){
		var data = Meteor.users.findOne(this.userId);
		if (!data)
			PostSubs.subscribe('userdata',{userId: this.userId});
		console.log('pro users', this.userId, data);
		return data;
	} */
});
Template.mailchimp.events({
  'click .submit' ( e, t ) {	
		var username = $('#username').val();
		console.log('submit username', $('#username').val());
		Meteor.call('user.addtester', {username: username, manage: 'add'});
  },
  'click .toggleTester' ( e, t ) {
		var tester, manage;
		console.log('toggleTester', e, this.roles, this );
		if (this.roles)
			tester = _.contains(_.flatten(_.values(this.roles)),'tester');
		if (tester) 
			manage = 'remove';
		else
			manage = 'add';
		Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },  
	'click .togglePro' ( e, t ) {
		var user, manage;
		var pro = ProUsers.findOne({userId: this._id});
		
		
		if (!pro)
			ProUsers.insert({userId: this._id, type: 'pro', max: Meteor.settings.public.ImageMaxPro, createdAt: new Date, expire: new Date(Date.now()+365*24*60*60*1000)});
		else
			ProUsers.remove(pro._id);
		pro = ProUsers.findOne({userId: this._id});
		console.log('togglePro', e, this.roles, this, 'pro:',  pro, Meteor.settings.public.ImageMaxPro);
		//Meteor.call('user.addtester', {userId: this._id, manage: manage});
		//Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },
	'click .fbmessenger'(e,t){
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked fbmessenger', this);
		Modal.show('sendMessageModal', this);
	},
	'click #showTesters'(e,t){
		var val = $('#showTesters').prop('checked');
		console.log('clicked showTesters', val, e.target, this);
		t.showtesters.set(val);
	},	
	'click #showPro'(e,t){
		var val = $('#showPro').prop('checked');
		console.log('clicked showPro', val, e.target, this);
		t.showpro.set(val);
	},
	'click .withTours'(e,t){
		t.showwith.set('withTours');
	},	
	'click .withNoPublishes'(e,t){
		t.showwith.set('withNoPublishes');
	},	
	'click .withNoPanos'(e,t){
		t.showwith.set('withNoPanos');
	},	
	'click .saveFile'(e,t){
		var json = JSON.stringify(t.file.get());
		var csv = convertToCSV(json);
		csv = 'Username, Email Address, First Name, Last Name\n' + csv;
		var fileout = new File([csv], 'filename', {type: 'contentType', lastModified: Date.now()}); 
		console.log('file', csv, '\n\n', json, '\n\n', t.file.get());
		saveAs( fileout, 'userlist.csv' );
	},
});

Template.prousers.onCreated( () => {
	AutoForm.debug(); 
	let t = Template.instance();
	t.subscribe('prousers',{debug: Session.get('debug')});
});
Template.prousers.helpers({
	pro(){
		var data = ProUsers.find({},{sort: {createdAt: -1}});
		console.log('pro users', data.fetch());
		return data;
	},
	userdata(){
		var data = Meteor.users.findOne(this.userId);
		//if (!data) PostSubs.subscribe('userdata',{userId: this.userId});
		console.log('pro users', this.userId, data);
		return data;
	}
});
Template.prousers.events({
  'click .submit' ( e, t ) {
		console.log('submit addpro', $('#addpro').val());
		var addpro = $('#addpro').val();
		Meteor.call('user.addpro', {username: addpro},function(err,res){
			if (res.error) Bert.alert(res.error, 'warning');
		});
  },
  'click .remove' ( e, t ) {
		console.log('remove addpro', e, this);
		ProUsers.remove(this._id);
  },
});

Template.testusers.onCreated( () => {
	AutoForm.debug(); 
	PostSubs.subscribe('userdata', {all: true});
});
Template.testusers.helpers({
	user(){
		var data = Meteor.users.find({'roles.adm-group':'tester'});
		console.log('test users', data.fetch());
		return data;
	},
/* 	userdata(){
		var data = Meteor.users.findOne(this.userId);
		if (!data)
			PostSubs.subscribe('userdata',{userId: this.userId});
		console.log('pro users', this.userId, data);
		return data;
	} */
});
Template.testusers.events({
  'click .submit' ( e, t ) {	
		var username = $('#username').val();
		console.log('submit username', $('#username').val());
		Meteor.call('user.addtester', {username: username, manage: 'add'});
  },
  'click .remove' ( e, t ) {
		console.log('remove username', e, this);
		Meteor.call('user.addtester', {userId: this._id, manage: 'remove'});
		//ProUsers.remove(this._id);
  },
});

Template.adminusers.onCreated( () => {
	AutoForm.debug(); 
	PostSubs.subscribe('userdata',{all: true});
});
Template.adminusers.helpers({
	admin(){
		var data = Meteor.users.find({'roles': {"$exists": true }});
		console.log('admin users', data.fetch());
		return data;
	},
/* 	userdata(){
		var data = Meteor.users.findOne(this.userId);
		return data;
	} */
});
Template.adminusers.events({
  'click .submit' ( e, t ) {
		console.log('submit addadmin', $('#addadmin').val());
		var addadmin = $('#addadmin').val();
		Meteor.call('user.admin', {username: addadmin, add: true, caller:'adminusers submit'});
		//PostSubs.subscribe('userdata',{all: true});
  },
  'click .removeadm' ( e, t ) {
		console.log('remove addadmin', this);
		if (Meteor.userId() == this._id)
			return Bert.alert('You can\'t remove yourself','warning');
		Meteor.call('user.admin', {username: this.username, remove: true, caller:'adminusers removeadm'});
		//PostSubs.subscribe('userdata',{all: true});
		//ProUsers.remove(this._id);
  },
});

Template.emailTemplates.onCreated( () => {
	AutoForm.debug(); 
	let t = Template.instance();
	t.editRecord = new ReactiveVar();
	t.subscribe('emailstmpl');
});
Template.emailTemplates.onRendered( () => { 
	let t = Template.instance();
	t.autorun(() => { 
		t.editRecord.get();
		if (window.CKEDITOR && $('textarea').attr('name')) 
			if (!$(this).hasClass('olark-form-message-input')) 
				$('textarea').each(function(){
					CKEDITOR.replace($(this).attr('name'))
				});
	});
});
Template.emailTemplates.helpers({
	editRecord(){
		return Template.instance().editRecord.get();
	},
  emails() {
		var data = EmailsTmpl.find();
		//console.log('uploaded tour', tour, Template.instance().tour.get());
		return data;
	},
  error() {
    return Template.instance().error.get();
  }
});
Template.emailTemplates.events({
	'click .pointer' (e,t){
		//e.preventDefault();
		//e.stopPropagation();
	},	
  'click .newRecord' ( e, t ) {
		e.preventDefault();
		e.stopPropagation();
		t.editRecord.set('new');
  },
  'click .editRecord' ( e, t ) {
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked editRecord', this);
		t.editRecord.set(this._id);
  },
  'submit' ( e, t ) {
		e.stopPropagation();
		console.log('submitting', e, this);
		t.editRecord.set();
  },
  'click .remove' ( e, t ) {
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked remove', this);
		EmailsTmpl.remove(this._id);
		t.editRecord.set();
		//template.editmarker.set();
  },
});

//import {Player } from 'player';

Template.soundsLibrary.onCreated(function () {
	var sub, tags;
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.vrloaded = new ReactiveVar();
	t.showtime = new ReactiveVar();
	t.sortPano = new ReactiveVar( 'date' );
	t.selectUsed = new ReactiveVar();
	t.selectTag = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.next = new ReactiveVar(6);
	t.limit = new ReactiveVar(6);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
  //Meteor.subscribe('tours');
	
	if (Session.get('sort'))
		t.sort.set(Session.get('sort'));	
	
	t.subscribe('showtime');
	//t.subscribe('googlefiles');
	t.subscribe('localfiles');
	t.subscribe('sounds');
	
	t.autorun(function(){
		var data = _LocalTags.findOne();
		if (data)
			tags = data.tags;
		sub = t.subscribe('sounds',{
//			userId: Meteor.userId(),
			unused: t.selectUsed.get(),
			limit: t.limit.get(),
			sort: t.sort.get(),
			tags: tags,
			debug: Session.get('debug')
		});
		t.ready.set(sub.ready());
		console.log('sounds.onCreated sub', tags, t.selectTag.get(), t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	})	
	t.autorun(function(){
		//t.ready.set(sub.ready());
		if (t.ready.get())
			t.loaded.set(Sounds.find().count());
		//if (Session.get('debug')) console.log('userpanos.onCreated sub ready', t.selectUsed.get(), t.loaded.get(), t.limit.get(), t.sort.get(), t.ready.get());
	})
	

});
Template.soundsLibrary.onRendered(function () {
	let t = Template.instance();	
	var callOptions;
	
	checkMore(t);
  $(window).scroll(function(){
		checkMore(t);
  });
	$('.bootstrap-tagsinput').find('input').attr('placeholder','tags');
	// HTTP.get('https://www.soundy.top/api/sounds', callOptions, function(err,res){
		// console.log('sounds.onRendered call', err, res);
	// });

});
Template.soundsLibrary.onDestroyed(function () {
	//$('#datepicker').datepicker('destroy');
});
Template.soundsLibrary.helpers({
	isMore(){
		let t = Template.instance();
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		if (Session.get('debug')) console.log('sounds isReady', t.ready.get(), 'vars:', t.selectUsed.get(), t.limit.get(), t.sort.get(), t.loaded.get() );
		return t.ready.get();
	},
	templatePagination() {
		var data = Template.instance().pagination;
		console.log('sounds pagination:', data.ready(), data); 
		return data;
	},
/* 	sounds() {
		var data = Template.instance().pagination.getPage();
		//if (Session.get('debug')) 
			console.log('sounds page', Template.instance().pagination.ready(), data, this, '\n\n'); 
		return data;
	}, */
	selectUsed(){
		let t = Template.instance();
		t.selectUsed.set(Session.get('selectUsed'));
		return t.selectUsed.get();
	},
	selectTag(){
		let t = Template.instance();
		var data = _LocalTags.findOne();
		if (data)
			return data.tags;
	},
	sorted(){
		var sort;
		if (Session.get('sort').createdAt)
			sort = 'date';
		else if (Session.get('sort').file)
			sort = 'filename';
		return sort;	
	},
	collection(){
		var data = Sounds;
		if (Session.get('debug')) console.log('sounds collection', data, Collections );
		return data;
	},
	schema(){
		return Schemas.SoundsServer;
	},
	sounds(){
		let t = Template.instance();
		var prouser, data, sort, tags;
		var list = {};
		if (!Meteor.userId())
			return;
		tags = _LocalTags.findOne();
		if (tags && tags.tags && tags.tags.length)
			list.tags = {$in: tags.tags};
		data = Sounds.find({library: true});
		if (Session.get('debug')) console.log('sounds helper', list.tags, data.count(), data.fetch() );
		return data;
	},	
	sounds2(){
		let t = Template.instance();
		var prouser, data, sort, tags;
		var list = {};
		if (!Meteor.userId())
			return;
		tags = _LocalTags.findOne();
		if (tags && tags.tags && tags.tags.length)
			list.tags = {$in: tags.tags};
		data = Sounds.find(list);
		if (Session.get('debug')) console.log('sounds helper', list.tags, data.count(), data.fetch() );
		return data;
	},
	subscribeFile(){
		let t = Template.instance();
		var sub = t.subscribe('localfiles',{_id: this.file});
		//console.log('subscribeFile sub', this.file, sub, sub.ready());
		return sub.ready();
	},
	localfile(){
		var data, sub;
		//sub = PostSubs.subscribe('localfiles', {_id: this.file});
		var data = LocalFiles.findOne(this.file);
		if (Session.get('debug')) console.log('sounds localfile', data, this);
		
		//if (data) console.log('sounds localfile', data.path, data.link());

		return data;
	},
	debug(){
		if (Session.get('debug')) console.log('sounds debug', this);
	}
});
Template.soundsLibrary.events({
	'click #infiniteCheck'(e,t){
		t.limit.set(t.limit.get() + t.next.get());
	},		
	'click .remove'(e,t){
		console.log('clicked remove', this._id, this);
		LocalFiles.remove(this.file);
		Sounds.remove(this._id);
	},			
	'click .selectTag'(e,t){
		t.selectTag.set(e.target.id);
		var tags = _LocalTags.findOne();
		var tag = e.target.id;
		var tag = tag.charAt(0).toLowerCase() + tag.slice(1);
		var Tag = tag.charAt(0).toUpperCase() + tag.slice(1);
		if (tags)
			_LocalTags.update(tags._id,{$addToSet:{tags: {$each: [tag, Tag]}}});
		else
			_LocalTags.insert({tags: [tag, Tag]});
	},	
	'click .removeTag'(e,t){
		var tags = _LocalTags.findOne();
		var tag = e.target.id;
		var Tag = tag.charAt(0).toUpperCase() + tag.slice(1);
		_LocalTags.update(tags._id,{$pull:{tags: tag}});
		_LocalTags.update(tags._id,{$pull:{tags: Tag}});
	}
});

Template.integration.onCreated(function () {
	var sub, tags;
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.vrloaded = new ReactiveVar();
	t.showtime = new ReactiveVar();
	t.sortPano = new ReactiveVar( 'date' );
	t.selectUsed = new ReactiveVar();
	t.selectTag = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.next = new ReactiveVar(6);
	t.limit = new ReactiveVar(6);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
  //Meteor.subscribe('tours');
	
	if (Session.get('sort'))
		t.sort.set(Session.get('sort'));	
	
});
Template.integration.onRendered(function () {
	Meteor.setTimeout(function(){
		$('.toggleSwitch').bootstrapToggle();
	},1000);
});
Template.integration.onDestroyed(function () {
	//$('#datepicker').datepicker('destroy');
});
Template.integration.helpers({
	isMore(){
		let t = Template.instance();
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		if (Session.get('debug')) console.log('sounds isReady', t.ready.get(), 'vars:', t.selectUsed.get(), t.limit.get(), t.sort.get(), t.loaded.get() );
		return t.ready.get();
	},
	libraries(){
		var data, sub;
		var data = Collections.Settings.find({$or: [{common: true}, {personal: {$exists: false}}]});
		console.log('integration', data.count(), data.fetch());
		return data;
	},
	enabled(){
		if (this.enable)
			return 'checked';
	},
	debug(){
		if (Session.get('debug')) console.log('sounds debug', this);
	}
});
Template.integration.events({
	'change .enableLib'(e,t){
		console.log('clicked enableLib', this.type, e.target.checked, '\n', e, this);
		Meteor.call('settings.set', {_id: this._id, type: this.type, enable: e.target.checked, common: true});
	},		
});

Template.verrors.onCreated(function () {
	var sub, tags;
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.vrloaded = new ReactiveVar();
	t.showtime = new ReactiveVar();
	t.sortPano = new ReactiveVar( 'date' );
	t.selectUsed = new ReactiveVar();
	t.selectTag = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.next = new ReactiveVar(6);
	t.limit = new ReactiveVar(6);
	t.sort = new ReactiveVar({createdAt: -1});
	t.loaded = new ReactiveVar(0);
  //Meteor.subscribe('tours');
	
	if (Session.get('sort'))
		t.sort.set(Session.get('sort'));	
	Meteor.subscribe('verrors');
	
});
Template.verrors.onRendered(function () {
	Meteor.setTimeout(function(){
		$('.toggleSwitch').bootstrapToggle();
	},1000);
});
Template.verrors.onDestroyed(function () {
	//$('#datepicker').datepicker('destroy');
});
Template.verrors.helpers({
	isMore(){
		let t = Template.instance();
		if (t.loaded.get() < 8 && t.limit.get() < 16)
			return true;
		return (t.limit.get() - t.next.get() <= t.loaded.get());
	},
	isReady() {
		let t = Template.instance();
		if (Session.get('debug')) console.log('sounds isReady', t.ready.get(), 'vars:', t.selectUsed.get(), t.limit.get(), t.sort.get(), t.loaded.get() );
		return t.ready.get();
	},
	verrors(){
		var data, sub;
		var data = Collections.Verrors.find({},{sort: {createdAt: -1}, limit: 20});
		console.log('verrors', data.count(), data.fetch());
		return data;
	},
	debug(){
		if (Session.get('debug')) console.log('sounds debug', this);
	}
});
Template.verrors.events({
	'click .delete'(e,t){
		Meteor.call('internal.verror.delete', {id: this._id});
	}
});

