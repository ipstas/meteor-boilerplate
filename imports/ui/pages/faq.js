import './faq.html';
import '../stylesheets/animation.css';
import { Faq } from '/imports/api/links/collections.js';


Template.faq.onCreated(function () {
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar();
	if (!FlowRouter.getQueryParam('select')) FlowRouter.setQueryParams({select: 'faqtext'});
	t.selector = new ReactiveVar(FlowRouter.getQueryParam('select'));
	t.editselector = new ReactiveVar();
	t.selectTag = new ReactiveVar();
	var sub;
	t.autorun(()=>{
		sub = t.subscribe('faq');
		t.ready.set(sub.ready());
	});
	t.autorun(function(){
		if (t.ready.get())
			window.prerenderReady = true;		
	});	
	//t.subscribe('feedback');
});
Template.faq.onRendered(function () {
	let t = Template.instance();
	//t.selector.set(FlowRouter.getQueryParam('get'));
});
Template.faq.helpers({
	ready(){
		let t = Template.instance();
		return t.ready.get();
	},
	isAdmin(){
		return (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group')) ;
	},
	selector(){
		// let t = Template.instance();
		// console.log('selector', t.selector.get());
		// return t.selector.get();
		return FlowRouter.getQueryParam('select');
	},
	ifpublished(){
		if (this.published)
			return true;		
		else
			return 'no';
	},
	feedback(){
		var feedback = Feedback.find({tourId:this.id});
		if (!feedback.count())
			Meteor.call('tour.feedback', this);
		return feedback;	
	},
});
Template.faq.events({
	'click .pointer': function (e, t) {
		if (!e.target.id) return;
		t.selector.set(e.target.id);
		console.log('clicked cursor', e.target.id, this);		
		FlowRouter.setQueryParams({select: e.target.id});
		
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				$('textarea').each(function(){
					CKEDITOR.replace($(this).attr('name'))
				});
		},500);
	},
	'click .askFaq'(e,t){
		if (!Meteor.userId())
			return Bert.alert('You have to be logged in to add a question', 'warning');
		Modal.show('askFaqModal', this);
	},
	'click .soon'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
});

Template.faqtext.onCreated(function () {
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.selector = new ReactiveVar(FlowRouter.getQueryParam('get'));
	t.editselector = new ReactiveVar();
	t.selectTag = new ReactiveVar();
  t.subscribe('faq');
	//t.subscribe('feedback');
});
Template.faqtext.onRendered(function () {
	let t = Template.instance();
	if (window.CKEDITOR && $('textarea').attr('name')) 
		$('textarea').each(function(){
			CKEDITOR.replace($(this).attr('name'))
		});
	//t.selector.set(FlowRouter.getQueryParam('get'));
});
Template.faqtext.helpers({
	ready(){
		let t = Template.instance();
		return t.ready.get();
	},
	isAdmin(){
		return (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group')) ;
	},
	selector(){
		//let t = Template.instance();
		//console.log('selector', t.selector.get());
		//return t.selector.get();
		return FlowRouter.getQueryParam('get');
	},
	editselector(){
		let t = Template.instance();
		return t.editselector.get();
	},	
	faq(){
		let t = Template.instance();
		var list = {published: true, $or: [{type: 'text'},{type: {$exists: false}}]}; 
		var data;
		if (t.selectTag.get())
			list.tags = {$in: t.selectTag.get()};
		if (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group'))
			list = {$or: [{type: 'text'},{type: {$exists: false}}]};
		data = Faq.find(list,{sort: {createdAt: 1}});
		console.log('fag fetched', data.fetch());
		return data;
	},
	grayscale(){
		if (!this.published)
			return 'gray grayscale';
	},
	ifpublished(){
		if (this.published)
			return true;		
		else
			return 'no';
	},
	feedback(){
		var feedback = Feedback.find({tourId:this.id});
		if (!feedback.count())
			Meteor.call('tour.feedback', this);
		return feedback;	
	},
	created(){
		return this.createdAt;
	}
});
Template.faqtext.events({
	'click .pointer': function (e, t) {
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked cursor', e.target.id, this);
		
		if (this.title != t.selector.get())
			t.selector.set(this.title);	
		else
			t.selector.set();
		
		FlowRouter.setQueryParams({get: t.selector.get()});
		
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				if (!$(this).hasClass('olark-form-message-input')) 
					$('textarea').each(function(){
						CKEDITOR.replace($(this).attr('name'))
					});
		},500);
	},
	'click .editselector'(e,t){
		t.editselector.set(e.target.id);
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				if (!$(this).hasClass('olark-form-message-input')) 
					$('textarea').each(function(){
						console.log('faqtext click .editselector attaching ckeditor', $(this).attr('name'));
						CKEDITOR.replace($(this).attr('name'))
					});
		},500);
	},	
	'click .removeselector'(e,t){
		Faq.remove(e.target.id);
	},
	'submit' (e,t){
		console.log('submitting', e, this);
		t.editselector.set();
	},
	'click .askFaq'(e,t){
		if (!Meteor.userId())
			return Bert.alert('You have to be logged in to add a question', 'warning');
		Modal.show('askFaqModal', this);
	},
	'click #commented'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
	'click #loved'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
	'click .soon'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
});

Template.faqvideo.onCreated(function () {
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.selector = new ReactiveVar(FlowRouter.getQueryParam('get'));
	t.editselector = new ReactiveVar();
	t.selectTag = new ReactiveVar();
  t.subscribe('faq');
	//t.subscribe('feedback');
});
Template.faqvideo.onRendered(function () {
	let t = Template.instance();
	//t.selector.set(FlowRouter.getQueryParam('get'));
});
Template.faqvideo.helpers({
	ready(){
		let t = Template.instance();
		return t.ready.get();
	},
	isAdmin(){
		return (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group')) ;
	},
	selector(){
		return FlowRouter.getQueryParam('get');
	},
	editselector(){
		let t = Template.instance();
		return t.editselector.get();
	},	
	faq(){
		let t = Template.instance();
		var list = {$and: [{published: true}, {type: 'video'}]}; 
		var data;
		if (t.selectTag.get())
			list.tags = {$in: t.selectTag.get()};
		if (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group'))
			list = {type: 'video'};
		data = Faq.find(list,{sort: {createdAt: 1}});
		console.log('fag fetched', data.fetch());
		return data;
	},
	grayscale(){
		if (!this.published)
			return 'gray grayscale';
	},
	ifpublished(){
		if (this.published)
			return true;		
		else
			return 'no';
	},
	feedback(){
		var feedback = Feedback.find({tourId:this.id});
		if (!feedback.count())
			Meteor.call('tour.feedback', this);
		return feedback;	
	},
	created(){
		return this.createdAt;
	}
});
Template.faqvideo.events({
	'click .pointer': function (e, t) {
		e.preventDefault();
		e.stopPropagation();
		console.log('clicked cursor', e.target.id, this);
		
		if (this.title != t.selector.get())
			t.selector.set(this.title);	
		else
			t.selector.set();
		
		FlowRouter.setQueryParams({get: t.selector.get()});
		
		Meteor.setTimeout(function(){
			if (window.CKEDITOR && $('textarea').attr('name')) 
				$('textarea').each(function(){
					CKEDITOR.replace($(this).attr('name'))
				});
		},500);
	},
	'click .editselector'(e,t){
		t.editselector.set(e.target.id);
	},	
	'click .removeselector'(e,t){
		Faq.remove(e.target.id);
	},
	'submit' (e,t){
		t.editselector.set();
	},
	'click .askFaq'(e,t){
		if (!Meteor.userId())
			return Bert.alert('You have to be logged in to add a question', 'warning');
		Modal.show('askFaqModal', this);
	},
	'click #commented'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
	'click #loved'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
	'click .soon'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
});

Template.faqstatus.onCreated(function () {
	let t = Template.instance();
  t.state = new ReactiveDict();
	t.hidden = new ReactiveVar();
	t.ready = new ReactiveVar();
	t.selector = new ReactiveVar(FlowRouter.getQueryParam('get'));
	t.editselector = new ReactiveVar();
	t.selectTag = new ReactiveVar();
	t.pullstatus = new ReactiveVar([]);
	t.setstatus = new ReactiveVar();

  t.subscribe('status');
	Meteor.call('social.twitter.pulstatus',function(e,r){
		t.pullstatus.set(r);
	});
	//t.subscribe('feedback');
});
Template.faqstatus.onRendered(function () {
	let t = Template.instance();
	//t.selector.set(FlowRouter.getQueryParam('get'));
	Meteor.setTimeout(function(){
		$('.toggleSwitch').bootstrapToggle({
      on: 'All',
      off: 'Status'
    });
	},500);
});
Template.faqstatus.helpers({
	loaded(){
		let t = Template.instance();
		if (t.setstatus.get())
			list = {};
		else
			list = {'entities.hashtags.text': 'status'};
		return Collections.Status.find().count();
	},
	ready(){
		let t = Template.instance();
		return t.ready.get();
	},
	isAdmin(){
		return (Roles.userIsInRole(Meteor.userId(), ['admin'], 'adm-group')) ;
	},
	selector(){
		let t = Template.instance();
		//console.log('selector', t.selector.get());
		return t.selector.get();
	},
	editselector(){
		let t = Template.instance();
		return t.editselector.get();
	},	
	twitter(){
		let t = Template.instance();
		var list = {};
		
		//Session.set('tmp', t.pullstatus.get());
		if (t.setstatus.get())
			list = {'user.id_str': '858031391946280961'};
		else
			list = {'user.id_str': '858031391946280961', 'entities.hashtags.text': 'status'};
		list.is_quote_status = false;
		var data =  Collections.Status.find(list, {sort: {id: -1}});
		console.log('twitter status', data.count(), data.fetch(), list);
		return data;
		//return t.pullstatus.get();
	},
	grayscale(){
		if (!this.published)
			return 'gray grayscale';
	},
	ifpublished(){
		if (this.published)
			return true;		
		else
			return 'no';
	},
	created(){
		return this.createdAt;
	}
});
Template.faqstatus.events({
	'change .twitterStatus'(e,t){
		console.log('clicked twitterStatus', this.type, e.target.checked, '\n', e, this);
		t.setstatus.set(e.target.checked);
	},
	'click #notifyMe'(e,t){
		// Let's check if the browser supports notifications
		console.log('clicked notifyme',e,this);
		
		if (!("Notification" in window)) {
			alert("This browser does not support desktop notification");
		}

		// Let's check whether notification permissions have already been granted
		else if (Notification.permission === "granted") {
			// If it's okay let's create a notification
			
			var notification = new Notification("Hi there!");
			console.log('notification granted',this, notification, Notification.permission);
		}

		// Otherwise, we need to ask the user for permission
		else if (Notification.permission !== "denied") {
			Notification.requestPermission(function (permission) {
				// If the user accepts, let's create a notification
				if (permission === "granted") {
					var notification = new Notification("Hi there!");
				}
			});
		}

		// At last, if the user has denied notifications, and you 
		// want to be respectful there is no need to bother them any more.		
	},
	'click .soon'(e,t){
		Bert.alert('this feature is coming soon', 'info');
	},
	
});
