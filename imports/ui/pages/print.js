import './print.html';
import '../stylesheets/json.css';

/* Template.registerHelper( 'json2txt', function ( json ) {

	var txt = ( json ) ? json2html.transform( json ) : json;
	var json2 = JSON.stringify(json);
	console.log('json2txt', json, json2);
	return 
	//return new Spacebars.SafeString( json );
}); */
	
Template.print.onCreated(function () {
  // this.state = new ReactiveDict();
	// this.vrloaded = new ReactiveVar();
  Meteor.subscribe('tours');
	
});

Template.print.onRendered(function () {
	Tracker.autorun(function () {
		var tourId = FlowRouter.current().params.id;
		//var tour = Tours.findOne({id: tourId});
		var tour = Tours.findOne({id: tourId});
		if (!tour)
			return;
		var json = JSON.stringify(tour);
		var file = new File([json], 'filename', {type: 'contentType', lastModified: Date.now()});
		if (FlowRouter.current().queryParams && FlowRouter.current().queryParams.print) 
			saveAs( file, FlowRouter.current().params.id + '.json' );
		console.log('exporting', tour, FlowRouter.current().params.id);
	});
});

Template.print.helpers({
	host(){
		return window.location.host;
	},
	tourId() {
		return FlowRouter.current().params.id;
	},
  tour() { 
//    return _getDataFromCollection( Tours, {}, {} ); 
		var _id = FlowRouter.current().params.id;
		//var tour = Tours.findOne({id: tourId});
		var data = Tours.findOne({_id: _id});
		if (!data)
			return;
		console.log('JSON html', _id, data);
		return data;
  },
	json2(){
		var _id = FlowRouter.current().params.id;
		//var tour = Tours.findOne({id: tourId});
		var data = Tours.findOne({_id: _id});
		if (!data)
			return;
		var json2 = JSON.stringify(data);
		console.log('JSON txt', _id, data, json2);
		return json2
	}
});

Template.print.events({
  'click .export-data' ( event, template ) {


		var tourId = FlowRouter.current().params.id;
		//var tour = Tours.findOne({id: tourId});
		var tour = Tours.findOne({id: tourId});
		var json = JSON.stringify(tour);
		var file = new File([json], 'filename', {type: 'contentType', lastModified: Date.now()});
		saveAs( file, FlowRouter.current().params.id + '.json' );
		console.log('exporting', tour);
		 event.preventDefault();

		//$( event.target ).button( 'reset' );
    // $( event.target ).button( 'loading' );

/*     let user        = Meteor.user(),
        fileName    = `${user.profile.name.first} ${user.profile.name.last}`,
        profileHtml = Modules.client.getProfileHTML();

    Meteor.call( 'exportData', profileHtml, ( error, response ) => {
      if ( error ) {
        Bert.alert( error.reason, 'warning' );
      } else {
        if ( response ) {
          // We'll handle the download here.
        }
      }
    }); */
  }
});

Template.printreal.onCreated(function () {
  // this.state = new ReactiveDict();
	// this.vrloaded = new ReactiveVar();
  Meteor.subscribe('tours');
	Meteor.subscribe('showtime');
	
});

Template.printreal.onRendered(function () {
	Tracker.autorun(function () {
		var username, tours;
		if (!FlowRouter.current().queryParams) {	
			tours = Showtime.find({},{fields: {ip: false}});
		} else if (FlowRouter.current().queryParams.master){
			username = FlowRouter.current().queryParams.master;
			tours = Showtime.find({master:username},{fields: {ip: false}});
		} else if (FlowRouter.current().queryParams.user){
			username = FlowRouter.current().queryParams.user;
			tours = Showtime.find({slave:username},{fields: {ip: false}});
		} else {
			tours = Showtime.find({},{fields: {ip: false}});
		}
		console.log('showing tours onRendered', tours.fetch(), tours.count());
		if (!tours.count() )
			return;
		var json = JSON.stringify(tours.fetch());
		var file = new File([json], 'filename', {type: 'contentType', lastModified: Date.now()}); 
		if (FlowRouter.current().queryParams && FlowRouter.current().queryParams.print) 
			saveAs( file, 'showtime.json' );
	});
});

Template.printreal.helpers({
/* 	tourId() {
		return FlowRouter.current().params.id;
	}, */
	host(){
		return window.location.host;
	},
  tours() { 
		//var tour = Tours.findOne({id: tourId});
		var tours = Showtime.find({},{fields: {ip: false}});
		console.log('showing tours', tours.fetch(), tours.count());
		if (tours.count() )
			return tours;
  },
});

Template.printpublictours.onCreated(function () {
  // this.state = new ReactiveDict();
	// this.vrloaded = new ReactiveVar();
  Meteor.subscribe('tours');
	Meteor.subscribe('showtime');
	
});

Template.printpublictours.onRendered(function () {
	Tracker.autorun(function () {
		var username, tours;
		if (FlowRouter.current().queryParams.worldtours == 'all') 
			tours = Tours.find({published: true});
		else (FlowRouter.current().queryParams.worldtours)
			tours = Tours.find({published: true, user: FlowRouter.current().queryParams.worldtours});

		console.log('showing tours onRendered', tours.fetch(), tours.count());
		if (!tours.count() )
			return;
		var json = JSON.stringify(tours.fetch());
		var file = new File([json], 'filename', {type: 'contentType', lastModified: Date.now()}); 
		if (FlowRouter.current().queryParams.print) 
			saveAs( file, 'worldtours.json' );
	});
});

Template.printpublictours.helpers({
/* 	tourId() {
		return FlowRouter.current().params.id;
	}, */
	host(){
		return window.location.host;
	},
  tours() { 
		var username, tours;
		if (FlowRouter.current().queryParams.worldtours == 'all') 
			tours = Tours.find({published: true});
		else (FlowRouter.current().queryParams.worldtours)
			tours = Tours.find({published: true, user: FlowRouter.current().queryParams.worldtours});
		if (tours.count() )
			return tours;
  },
});

Template.printusertours.onCreated(function () {
  // this.state = new ReactiveDict();
	// this.vrloaded = new ReactiveVar();
  Meteor.subscribe('tours');
	Meteor.subscribe('showtime');
	
});

Template.printusertours.onRendered(function () {
	Tracker.autorun(function () {
		var username, tours;
		tours = Tours.find({user: FlowRouter.current().queryParams.usertours});

		console.log('showing tours onRendered', tours.fetch(), tours.count());
		if (!tours.count() )
			return;
		var json = JSON.stringify(tours.fetch());
		var file = new File([json], 'filename', {type: 'contentType', lastModified: Date.now()}); 
		if (FlowRouter.current().queryParams.print) 
			saveAs( file, 'worldtours.json' );
	});
});

Template.printusertours.helpers({
/* 	tourId() {
		return FlowRouter.current().params.id;
	}, */
	host(){
		return window.location.host;
	},
  tours() { 
		var username, tours;
		tours = Tours.find({user: FlowRouter.current().queryParams.usertours});
		if (tours.count() )
			return tours;
  },
});

Template.helper.helpers({
/* 	tourId() {
		return FlowRouter.current().params.id;
	}, */
	host(){
		return window.location.host;
	},
});


